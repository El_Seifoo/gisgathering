package com.gisgathering.neomit.seif.gisgathering.offline;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by sief on 7/30/2018.
 */

public class OfflinePresenter implements OfflineMVP.Presenter, OfflineModel.DBCallback, OfflineModel.VolleyCallback, OfflineModel.UploadingPhotoCallback {
    private OfflineMVP.View view;
    private OfflineModel model;
    private Activity activity;
    private boolean isSyncing;

    public OfflinePresenter(OfflineMVP.View view, OfflineModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void requestReadingsList(Activity activity, boolean isSyncing) {
        if (!isSyncing) model.getOfflineData(activity.getApplicationContext(), this, isSyncing);
        else {

            if (DataDbHelper.getmInstance(activity.getApplicationContext()).dataCount() > 0) {
                this.activity = activity;
                this.isSyncing = isSyncing;
                view.showProgress();
                model.syncData(activity.getApplicationContext(), this, DataDbHelper.getmInstance(activity.getApplicationContext()).getAllData());
            }
        }
    }

    @Override
    public void requestRemoveReadingById(Context context, int id) {
        isRemoving = true;
        model.removeOneItem(context, this, id, false);
    }

    @Override
    public void onActivityResult(int requestCode, int editDataRequestCode, int resultCode, Intent data) {
        if (requestCode == editDataRequestCode && resultCode == RESULT_OK && data != null) {
            view.updateDataItemView((Data) data.getSerializableExtra("UpdatedData"));
        }
    }


    boolean isRemoving;

    @Override
    public void onListItemClicked(Context context, Data data, View ClickedView) {
        switch (ClickedView.getId()) {
            case R.id.edit_data:
                view.editData(data);
                break;
            case R.id.remove_data:
                //if not removing ...
                if (!isRemoving)
                    this.requestRemoveReadingById(context, data.getId());
                break;
            case R.id.more_info:
                view.showMoreDetails(data);
                break;
            case R.id.sync_button:
                view.showProgress();
                model.syncRecord(context, this, data);
        }
    }


    @Override
    public void onResponse(Context context, ArrayList<Data> dataList, boolean isSyncing) {
        if (dataList.isEmpty()) view.showEmptyListText();
        else view.loadDataList(dataList);
    }

    @Override
    public void onRemovingSuccess(int id, boolean all) {
        isRemoving = false;
        view.updateList(id);
        if (all) this.requestReadingsList(activity, isSyncing);
    }

    // handling response of SyncData ...
    @Override
    public void onSuccess(Context context, String responseString, Data data, boolean all) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        JSONArray array = response.getJSONArray("response");
        if (array.getJSONObject(0).getString("status").equals("success")) {
            // data success upload its photos ...
//            if (!data.getPhotos().isEmpty())
            if ((data.getPhotos().get("meterConnectionFullView") != null && data.getPhotos().get("meterConnectionFullView").contains("//////"))
                    || (data.getPhotos().get("meterCloseUpView") != null && data.getPhotos().get("meterCloseUpView").contains("//////"))
                    || (data.getPhotos().get("electricMeterPhoto") != null && data.getPhotos().get("electricMeterPhoto").contains("//////"))
                    || (data.getPhotos().get("propertyPhoto") != null && data.getPhotos().get("propertyPhoto").contains("//////"))
                    || (data.getPhotos().get("amanaPlatePhoto") != null && data.getPhotos().get("amanaPlatePhoto").contains("//////"))
                    || (data.getPhotos().get("NWCPlatePhoto") != null && data.getPhotos().get("NWCPlatePhoto").contains("//////")))
                model.syncPics(context, this, data.getId(), data.getPhotos(), data.getPlateNumber(), all);
            else {
                view.hideProgress();
                model.removeOneItem(activity == null ? context : activity.getApplicationContext(), this, data.getId(), all);
            }
        } else {
            view.hideProgress();
            view.showToastMessage(array.getJSONObject(0).getString("message"));
            if (all) this.requestReadingsList(activity, isSyncing);
        }
    }

    @Override
    public void onFailed(Context context, VolleyError error, boolean all) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        if (error instanceof AuthFailureError) {
            view.showToastMessage(context.getString(R.string.your_session_ended_please_relogin));
            view.goLogin();
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }

    @Override
    public void onUploadSuccess(Context context, JSONArray response, int dataId, boolean all, ArrayList<String> paths) throws JSONException {
        view.hideProgress();
        for (int i = 0; i < response.length(); i++) {
            if (!response.getJSONObject(i).getString("status").equals("success")) {
                view.showToastMessage(context.getString(R.string.failed_to_sync_this_record));
                if (all) this.requestReadingsList(activity, isSyncing);
                return;
            }
        }
        for (int i = 0; i < paths.size(); i++) {
            deleteImage(context, paths.get(i));
        }
        model.removeOneItem(activity == null ? context : activity.getApplicationContext(), this, dataId, all);
    }

    public void deleteImage(Context context, String path) {
        Log.e("path", path);
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted 1:" + path);
                callBroadCast(context);
            } else {
                Log.e("-->", "file not Deleted 1:" + path);
            }
        }
    }

    public void callBroadCast(Context context) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            view.sendBroadCast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    @Override
    public void onUploadFailed(String message, boolean all) {
        view.hideProgress();
        view.showToastMessage(message);
        if (all) this.requestReadingsList(activity, isSyncing);
    }

    @Override
    public void noPicsToUpload(String message, boolean all) {
        view.hideProgress();
        view.showToastMessage(message);
        if (all) this.requestReadingsList(activity, isSyncing);
    }
}
