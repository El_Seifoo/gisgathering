package com.gisgathering.neomit.seif.gisgathering.login;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.home.HomeActivity;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;


public class LoginActivity extends AppCompatActivity implements LoginMVP.View {
    private EditText username, password, ip;
    private LoginMVP.Presenter presenter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle(getString(R.string.log_in));

        presenter = new LoginPresenter(this, new LoginModel());
        presenter.checkIfUserLoggedIn(MySingleton.getmInstance(getApplicationContext()).isLoggedIn());


        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        username = (EditText) findViewById(R.id.user_name_edit_text);
        password = (EditText) findViewById(R.id.password_edit_text);
        ip = (EditText) findViewById(R.id.ip_edit_text);
        username.setText(MySingleton.getmInstance(getApplicationContext()).getUserName());
        ip.setText(MySingleton.getmInstance(getApplicationContext()).getUserIP());
        ((Button) findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestLogin(getApplicationContext(), username.getText().toString().trim(),
                        password.getText().toString().trim(), ip.getText().toString().trim());
            }
        });

    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
