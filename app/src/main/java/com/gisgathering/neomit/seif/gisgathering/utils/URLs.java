package com.gisgathering.neomit.seif.gisgathering.utils;

/**
 * Created by sief on 7/26/2018.
 */

public class URLs {
    public static final String HTTP = "http://";
    public static final String ROOT_URL = "/api/gathering/";
    public static final String LOGIN = ROOT_URL + "login";
    public static final String GET_INFO = ROOT_URL + "getinfo";
    public static final String MAP = ROOT_URL + "map";
    public static final String SYNC_PHOTOS = ROOT_URL + "SyncImages";
    public static final String SYNC_DATA = ROOT_URL + "SyncData";
    public static final String DISTRICTS = ROOT_URL + "GetDistricts";
    public static final String REJECTED = ROOT_URL + "GetRejected";
    public static final String CHECK_PLATE_NUMBER = ROOT_URL + "checkplatenumber";
}
