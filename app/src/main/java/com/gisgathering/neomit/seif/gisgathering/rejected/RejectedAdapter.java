package com.gisgathering.neomit.seif.gisgathering.rejected;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

public class RejectedAdapter extends RecyclerView.Adapter<RejectedAdapter.Holder> {
    private ArrayList<Data> dataList;
    final private ListItemClickListener mOnClickListener;


    public void setDataList(ArrayList<Data> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public RejectedAdapter(ListItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public void clear() {
        if (dataList != null) {
            dataList.clear();
            notifyDataSetChanged();
        }
    }

    public interface ListItemClickListener {
        void onListItemClick(int position, Data data, View view);
    }

    public void removeReading(Context context, int position) {
        if (dataList != null) {
            dataList.remove(position);
            notifyItemRemoved(position);
            if (dataList.isEmpty())
                ((RejectedActivity) context).showEmptyListText();
        }
    }

    @NonNull
    @Override
    public RejectedAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rejected_data_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RejectedAdapter.Holder holder, int position) {
        String numbers = "";
        numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </font>" + dataList.get(position).getPlateNumber() + "<br>";
        if (!dataList.get(position).getHouseConnNumber().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.house_connection_number) + ": </font>" + dataList.get(position).getHouseConnNumber() + "<br>";
        if (!dataList.get(position).getAmrMeterBarcode().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.amr_meter_barcode) + ": </font>" + dataList.get(position).getAmrMeterBarcode() + "<br>";
        if (dataList.get(position).getReason() != null && !dataList.get(position).getReason().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.reason) + ": </font>" + dataList.get(position).getReason() + "<br>";

        holder.numbers.setText(Html.fromHtml(numbers.substring(0, numbers.length() - 4)), TextView.BufferType.SPANNABLE);
    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView numbers, moreDetails;

        public Holder(View itemView) {
            super(itemView);
            numbers = (TextView) itemView.findViewById(R.id.numbers);
            moreDetails = (TextView) itemView.findViewById(R.id.more_info);
            moreDetails.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onListItemClick(getAdapterPosition(), dataList.get(getAdapterPosition()), v);
        }

    }
}
