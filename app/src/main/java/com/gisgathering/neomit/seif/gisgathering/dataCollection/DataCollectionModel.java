package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.gisgathering.neomit.seif.gisgathering.utils.URLs;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by sief on 7/26/2018.
 */

public class DataCollectionModel {
    protected void save(Context context, DBCallback callback, Data data) {
        boolean flag = DataDbHelper.getmInstance(context).addNewData(data);
        callback.onResponse(context, flag);
    }

    protected void editData(Context context, DBCallback callback, Data data) {
        boolean flag = DataDbHelper.getmInstance(context).updateData(data);
        callback.onResponse(context, data, flag);
    }

    protected void syncRecord(final Context context, final VolleyCallback callback, final Data data, final boolean isRejected) {
        Log.e("IDID", data.getId() + "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, data, isRejected);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFailed(context, error, 0);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("Json[data][0][ID]", data.getId() + "");
                params.put("Json[data][0][PlateNo]", data.getPlateNumber());
                params.put("Json[data][0][HCN]", data.getHouseConnNumber().isEmpty() ? "-1" : data.getHouseConnNumber());
                params.put("Json[data][0][AmrMeterBarcode]", data.getAmrMeterBarcode().isEmpty() ? "-1" : data.getAmrMeterBarcode());
                params.put("Json[data][0][WaterMeterNumber]", data.getWaterMeterNumber().isEmpty() ? "-1" : data.getWaterMeterNumber());
                params.put("Json[data][0][WaterMeterReading]", data.getWaterMeterReading().isEmpty() ? "-1" : data.getWaterMeterReading());
                params.put("Json[data][0][RemarkID]", data.getRemarks() + "");
                params.put("Json[data][0][StatusID]", data.getWaterMeterStatus() + "");
                params.put("Json[data][0][MeterTypeID]", data.getMeterType() + "");
                params.put("Json[data][0][MeterDiameterID]", data.getMeterDiameter() + "");
                params.put("Json[data][0][ReducerDiameterID]", data.getReducerDiameter() + "");
                params.put("Json[data][0][OpenCloseID]", data.getOpenClose() + "");
                params.put("Json[data][0][MechElecID]", data.getMechElec() + "");
                params.put("Json[data][0][ValveTypeID]", data.getValveType() + "");
                params.put("Json[data][0][CoverExists]", data.getCoverExists() + "");
                params.put("Json[data][0][Position]", data.getPosition() + "");
                params.put("Json[data][0][MeterBoxTypeID]", data.getMeterTypeBox() + "");
                params.put("Json[data][0][PipeAfterMeterID]", data.getPipeAfterMeter() + "");
                params.put("Json[data][0][MeterInOutID]", data.getInsideOutside() + "");
                params.put("Json[data][0][PipeMaterialID]", data.getPipeMaterial() + "");
                params.put("Json[data][0][PipeSizeID]", data.getPipeSize() + "");
                params.put("Json[data][0][SewerConnExists]", data.getSewerConnection() + "");
                params.put("Json[data][0][NumberOfElectricMeters]", data.getNumberOfElectricMeters().isEmpty() ? "-1" : data.getNumberOfElectricMeters());
                params.put("Json[data][0][ElectricNumberLowest]", data.getElectricMeterNumberLowest().isEmpty() ? "-1" : data.getElectricMeterNumberLowest());
                params.put("Json[data][0][PostalCode]", data.getPostalCode().isEmpty() ? "-1" : data.getPostalCode());
                params.put("Json[data][0][FloorCount]", data.getNumberOfFloors().isEmpty() ? "-1" : data.getNumberOfFloors());
                params.put("Json[data][0][PremiseID]", data.getBuildingCategories() == 5 ? "6" : data.getBuildingCategories() + "");
                params.put("Json[data][0][SubBuildingCommercialID]", data.getCommercial() + "");
                params.put("Json[data][0][SubBuildingResidentialID]", data.getResidential() + "");
                params.put("Json[data][0][SubBuildingGovernmentID]", data.getGovernment() + "");
                params.put("Json[data][0][DistrictID]", data.getDistrict() + "");
                params.put("Json[data][0][WaselNumber]", data.getWaselNumber().isEmpty() ? "-1" : data.getWaselNumber());
                params.put("Json[data][0][AmanaBarcode]", data.getAmanaBarcode().isEmpty() ? "-1" : data.getAmanaBarcode());
                params.put("Json[data][0][Parcel_ID]", data.getParcelID().isEmpty() ? "-1" : data.getParcelID());
                params.put("Json[data][0][Old_Sec_Account]", data.getOldAccountNumber().isEmpty() ? "-1" : data.getOldAccountNumber());
                params.put("Json[data][0][partner_number]", data.getPartnerNumber().isEmpty() ? "-1" : data.getPartnerNumber());
                params.put("Json[data][0][Number_of_Water_Connections]", data.getNumberOfWaterConnections().isEmpty() ? "-1" : data.getNumberOfWaterConnections());
                params.put("Json[data][0][Number_of_Units]", data.getNumberOfUnits().isEmpty() ? "-1" : data.getNumberOfUnits());
                params.put("Json[data][0][XCoordinate]", !data.getLocation().isEmpty() ? data.getLocation().split(",")[0] : "-1");
                params.put("Json[data][0][YCoordinate]", !data.getLocation().isEmpty() ? data.getLocation().split(",")[1] : "-1");
                params.put("Json[data][0][ZoneCode]", !data.getZoneCode().isEmpty() ? data.getZoneCode() : "-1");
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void syncPics(final Context context, final UploadingPhotoCallback callback, Map<String, String> photos, String plateNumber, final boolean isRejected) {
        try {
            UploadService.NAMESPACE = "com.gisgathering.neomit.seif.gisgathering";
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, UUID.randomUUID().toString(), URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.SYNC_PHOTOS);

            final ArrayList<String> paths = new ArrayList<>();
            if (photos.get("meterConnectionFullView") != null && photos.get("meterConnectionFullView").contains("//////")) {
                Log.e("meterConnectionFullView", photos.get("meterConnectionFullView").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("meterConnectionFullView").split("//////")[1], plateNumber + "_full");
                paths.add(photos.get("meterConnectionFullView").split("//////")[1]);

            }
            if (photos.get("meterCloseUpView") != null && photos.get("meterCloseUpView").contains("//////")) {
                Log.e("meterCloseUpView", photos.get("meterCloseUpView").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("meterCloseUpView").split("//////")[1], plateNumber + "_close");
                paths.add(photos.get("meterCloseUpView").split("//////")[1]);
            }
            if (photos.get("electricMeterPhoto") != null && photos.get("electricMeterPhoto").contains("//////")) {
                Log.e("electricMeterPhoto", photos.get("electricMeterPhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("electricMeterPhoto").split("//////")[1], plateNumber + "_elec");
                paths.add(photos.get("electricMeterPhoto").split("//////")[1]);
            }
            if (photos.get("propertyPhoto") != null && photos.get("propertyPhoto").contains("//////")) {
                Log.e("propertyPhoto", photos.get("propertyPhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("propertyPhoto").split("//////")[1], plateNumber + "_property");
                paths.add(photos.get("propertyPhoto").split("//////")[1]);
            }
            if (photos.get("amanaPlatePhoto") != null && photos.get("amanaPlatePhoto").contains("//////")) {
                Log.e("amanaPlatePhoto", photos.get("amanaPlatePhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("amanaPlatePhoto").split("//////")[1], plateNumber + "_amana");
                paths.add(photos.get("amanaPlatePhoto").split("//////")[1]);
            }
            if (photos.get("NWCPlatePhoto") != null && photos.get("NWCPlatePhoto").contains("//////")) {
                Log.e("NWCPlatePhoto", photos.get("NWCPlatePhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("NWCPlatePhoto").split("//////")[1], plateNumber + "_nwc");
                paths.add(photos.get("NWCPlatePhoto").split("//////")[1]);
            }


            multipartUploadRequest
                    .addHeader("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken())
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(0)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {

                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
                            Log.e("error", exception.toString());
                        }

                        @Override
                        public void onCompleted(Context context1, UploadInfo uploadInfo, ServerResponse serverResponse) {
                            try {
//                                JSONObject response = new JSONObject(new String(serverResponse.getBody(), "UTF-8"));
                                JSONArray array = new JSONArray(new String(serverResponse.getBody(), "UTF-8"));
                                Log.e("server response", array.toString());
                                callback.onUploadSuccess(context, array, isRejected, paths);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("json", e.toString());
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Log.e("unsupportedEncoding", e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record));
                        }
                    })
                    .startUpload();
        } catch (Exception e) {
            Log.e("e", e.toString());
            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record));
        }
    }

    protected void getDistricts(final Context context, final VolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.DISTRICTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess1(context, response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFailed(context, error, 1);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void checkPlateNumber(final Context context, final VolleyCallback callback, final String plateNumber) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.CHECK_PLATE_NUMBER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onCheckSuccess(context, response, plateNumber);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                // do nothing ...
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("Json[PlateNo]", plateNumber);
                }};
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface DBCallback {
        // add new data response ...
        void onResponse(Context context, boolean flag);

        // edit data response ...
        void onResponse(Context context, Data data, boolean flag);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String responseString, Data data, boolean isRejected) throws JSONException;

        void onFailed(Context context, VolleyError error, int index);

        void onSuccess1(Context context, String responseString) throws JSONException;

        void onCheckSuccess(Context context, String response, String plateNumber) throws JSONException;

    }

    protected interface UploadingPhotoCallback {
        void onUploadSuccess(Context context, JSONArray response, boolean isRejected, ArrayList<String> paths) throws JSONException;

        void onUploadFailed(String message);


    }


}
