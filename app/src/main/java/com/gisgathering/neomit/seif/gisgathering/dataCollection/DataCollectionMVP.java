package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by sief on 7/26/2018.
 */

public interface DataCollectionMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void loadEditingDataEditTexts(Data data, String[] editTextsData, boolean[] flags);

        void setEditingImages(ImageView imageViews, Bitmap path);

        void setEditingImages(ImageView imageViews, String path);

        void loadDistricts(ArrayList<District> districts);

        void setSpinnersData(Spinner spinner, ArrayAdapter<String> adapter, int position);

        void setSpinnersData(Spinner spinner, ArrayAdapter<String> adapter);

        void showToastMessage(String message);

        void initializeScanner(IntentIntegrator intentIntegrator, int flag);

        void setQRData(EditText editText, String data);

        void startPickingPhotoForResult(Intent intent, int request);

        void loadSelectedPic(byte[] image, ImageView imageView);

        void onSaveReadingSuccess(String message);

        void onEditReadingSuccess(String message, Data data);

        void onRejectedUpdated();

        void setLocation(String location);

        void showHideBuildingCategs(boolean comm, boolean res, boolean gov, boolean tanker);

        int getId();

        int getDistrictId();

        String getDistrictName();

        void goLogin();

        void goGetInfo(String plateNumber);

        void clearFocus();

        void sendBroadCast(Intent intent);
    }

    interface Presenter {

        void requestEditingData(Activity activity, boolean isEditing);

        void requestEditingPhotos(Map<String, String> photos, ImageView[] imageViews, boolean isSynced, boolean isRejected);

        void requestDistricts(Context context);

        void requestAllSpinnersList(Activity activity, Spinner[] spinners, String[][] allLists, boolean isEditing);

        void whichView(Activity activity, android.view.View view, boolean isPickingImage);

        void requestQRScanner(Activity activity, String editText);

        void handleQRScannerResult(String data, int flag, EditText[] editTexts);

        void onRequestPermissionsResult(Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults, ImageView[] imageViews);

        void onActivityResult(Context context, int requestCode, int resultCode, Intent data, ImageView[] imageViews, EditText[] editTexts);

        void preRequestSaveData(Context context, EditText[] editTexts, Spinner[] spinners, String location, boolean isSaving, boolean isSynced);

        void preRequestSaveData(Context context, EditText[] editTexts, Spinner[] spinners, String location);

        void requestSaveData(Context context, Data data, boolean isSaving, boolean isSynced);

        void requestSaveData(Context context, Data data);

        void requestAddNewLocation(Activity activity);

        void requestCheckPlateNumber(Context context, String plateNumber);
    }
}
