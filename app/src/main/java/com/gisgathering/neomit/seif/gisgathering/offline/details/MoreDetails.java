package com.gisgathering.neomit.seif.gisgathering.offline.details;

import java.io.Serializable;

/**
 * Created by sief on 7/30/2018.
 */

public class MoreDetails implements Serializable {
    String label;
    String value;

    public MoreDetails(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
