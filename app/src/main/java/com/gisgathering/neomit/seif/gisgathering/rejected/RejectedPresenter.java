package com.gisgathering.neomit.seif.gisgathering.rejected;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class RejectedPresenter implements RejectedMVP.Presenter, RejectedModel.VolleyCallback {
    private RejectedMVP.View view;
    private RejectedModel model;

    public RejectedPresenter(RejectedMVP.View view, RejectedModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestReadingsList(Context context) {
        view.showProgress();
        model.getRejected(context, this);
    }

    @Override
    public void search(ArrayList<Data> dataList, String query) {
        if (dataList == null) return;
        if (query.isEmpty()) {
            view.loadSearchedData(dataList);
            return;
        }

        ArrayList<Data> searchedList = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getPlateNumber().startsWith(query))
                searchedList.add(dataList.get(i));
        }

        if (searchedList.isEmpty())
            view.showEmptyListText();
        else
            view.loadSearchedData(searchedList);

    }

    @Override
    public void onSuccess(Context context, String responseString) throws JSONException {
        view.hideProgress();
        ArrayList<Data> rejectList = new ArrayList<>();
        JSONObject response = new JSONObject(responseString);
        if (response.has("status")) {
            view.showEmptyListText();
            return;
        }
        JSONArray data = response.getJSONArray("data");
        Map<String, String> photos = null;
        int buildingCat;
        for (int i = 0; i < data.length(); i++) {
            photos = new LinkedHashMap<>();
            photos.put("meterConnectionFullView", data.getJSONObject(i).getString("full_img"));
            photos.put("meterCloseUpView", data.getJSONObject(i).getString("close_img"));
            photos.put("electricMeterPhoto", data.getJSONObject(i).getString("elec_img"));
            photos.put("propertyPhoto", data.getJSONObject(i).getString("property_img"));
            photos.put("amanaPlatePhoto", data.getJSONObject(i).getString("amana_img"));
            photos.put("NWCPlatePhoto", data.getJSONObject(i).getString("nwc_img"));
            buildingCat = returnValidInt(data.getJSONObject(i).getString("PremiseID"));
            rejectList.add(new Data(data.getJSONObject(i).getInt("ID"), returnValidString(data.getJSONObject(i).getString("PlateNo")),
                    returnValidString(data.getJSONObject(i).getString("HCN")), returnValidString(data.getJSONObject(i).getString("AmrMeterBarcode")),
                    returnValidString(data.getJSONObject(i).getString("WaterMeterNumber")), returnValidString(data.getJSONObject(i).getString("WaterMeterReading")),
                    returnValidInt(data.getJSONObject(i).getString("RemarkID")), returnValidInt(data.getJSONObject(i).getString("StatusID")),
                    returnValidInt(data.getJSONObject(i).getString("MeterTypeID")), returnValidInt(data.getJSONObject(i).getString("MeterDiameterID")),
                    returnValidInt(data.getJSONObject(i).getString("ReducerDiameterID")), returnValidInt(data.getJSONObject(i).getString("OpenCloseID")),
                    returnValidInt(data.getJSONObject(i).getString("MechElecID")), returnValidInt(data.getJSONObject(i).getString("ValveTypeID")),
                    returnValidInt(data.getJSONObject(i).getString("CoverExists")), returnValidInt(data.getJSONObject(i).getString("Position")),
                    returnValidInt(data.getJSONObject(i).getString("MeterBoxTypeID")), returnValidInt(data.getJSONObject(i).getString("PipeAfterMeterID")),
                    returnValidInt(data.getJSONObject(i).getString("MeterInOutID")), returnValidInt(data.getJSONObject(i).getString("PipeMaterialID")),
                    returnValidInt(data.getJSONObject(i).getString("PipeSizeID")), returnValidInt(data.getJSONObject(i).getString("SewerConnExists")),
                    returnValidString(data.getJSONObject(i).getString("NumberOfElectricMeters")), returnValidString(data.getJSONObject(i).getString("ElectricNumberLowest")),
                    returnValidString(data.getJSONObject(i).getString("PostalCode")), returnValidString(data.getJSONObject(i).getString("FloorCount")),
                    buildingCat == 6 ? 5 : buildingCat, returnValidInt(data.getJSONObject(i).getString("SubBuildingCommercialID")),
                    returnValidInt(data.getJSONObject(i).getString("SubBuildingResidentialID")), returnValidInt(data.getJSONObject(i).getString("SubBuildingGovernmentID")),
                    returnValidInt(data.getJSONObject(i).getString("DistrictID")), returnValidString(data.getJSONObject(i).getString("WaselNumber")),
                    returnValidString(data.getJSONObject(i).getString("AmanaBarcode")), returnValidString(data.getJSONObject(i).getString("Parcel_ID")),
                    returnValidString(data.getJSONObject(i).getString("Old_Sec_Account")), returnValidString(data.getJSONObject(i).getString("partner_number")),
                    returnValidString(data.getJSONObject(i).getString("Number_of_Water_Connections")), returnValidString(data.getJSONObject(i).getString("Number_of_Units")),
                    returnValidString(data.getJSONObject(i).getString("ZoneCode")),
                    returnValidString(data.getJSONObject(i).getString("XCoordinate")) + "," + returnValidString(data.getJSONObject(i).getString("YCoordinate")),
                    returnValidString(data.getJSONObject(i).getString("Reason")), photos, ""));
            //returnValidString(data.getJSONObject(0).getString("partner_number"))
        }

        view.loadRejectedList(rejectList);

    }

    private int returnValidInt(String data) {
        return data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty() ? -1 : Integer.valueOf(data);
    }

    private String returnValidString(String data) {
        return data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty() || data.equals("-1") ? "" : data;
    }

    @Override
    public void onFailed(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        if (error instanceof AuthFailureError) {
            view.showToastMessage(context.getString(R.string.your_session_ended_please_relogin));
            view.goLogin();
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));

    }
}
