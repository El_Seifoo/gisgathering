package com.gisgathering.neomit.seif.gisgathering.offline.details;

import android.content.Context;

import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public interface MoreDetailsMVP {
    interface View {
        void loadData(ArrayList<MoreDetails> moreDetailsList, boolean hasImgs, int count);
    }

    interface Presenter {
        void requestSpinnersStrings(Context context, Data data);
    }
}
