package com.gisgathering.neomit.seif.gisgathering.infoMap;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.gisgathering.neomit.seif.gisgathering.utils.URLs;

import org.json.JSONException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sief on 8/8/2018.
 */

public class InfoMapModel {
    protected void getInfo(final Context context, final VolleyCallback callback, final String plateNumber) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.GET_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("Json[PlateNo]", plateNumber);
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void locate(final Context context, final VolleyCallback callback, final String plateNumber) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.MAP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("Json[PlateNo]", plateNumber);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected interface VolleyCallback {
        void onSuccess(Context context, String response, int step) throws JSONException;

        void onFail(Context context, VolleyError error);
    }
}
