package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.util.Log;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by sief on 7/29/2018.
 */

public class Data implements Serializable {
    private int id;
    private String plateNumber;
    private String houseConnNumber;
    private String amrMeterBarcode;
    private String waterMeterNumber;
    private String waterMeterReading;//dot
    private int remarks;
    private int waterMeterStatus;
    private int meterType;
    private int meterDiameter;
    private int reducerDiameter;
    private int openClose;
    private int mechElec;
    private int valveType;
    private int coverExists;
    private int position;
    private int meterTypeBox;
    private int pipeAfterMeter;
    private int insideOutside;
    private int pipeMaterial;
    private int pipeSize;
    private int sewerConnection;
    private String numberOfElectricMeters;
    private String electricMeterNumberLowest;
    private String postalCode;
    private String numberOfFloors;
    private int BuildingCategories;
    private int commercial;
    private int residential;
    private int government;
    private int district;
    private String waselNumber;
    private String amanaBarcode;
    private String parcelID;
    private String oldAccountNumber;
    private String partnerNumber;
    private String numberOfWaterConnections;
    private String numberOfUnits;
    private String location;
    private String reason;
    private String zoneCode;
    private Map<String, String> photos;

    private String districtName;


    public Data(String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, String zoneCode, String location, Map<String, String> photos) {
        this.plateNumber = plateNumber;
        this.houseConnNumber = houseConnNumber;
        this.amrMeterBarcode = amrMeterBarcode;
        this.waterMeterNumber = waterMeterNumber;
        this.waterMeterReading = waterMeterReading;
        this.remarks = remarks;
        this.waterMeterStatus = waterMeterStatus;
        this.meterType = meterType;
        this.meterDiameter = meterDiameter;
        this.reducerDiameter = reducerDiameter;
        this.openClose = openClose;
        this.mechElec = mechElec;
        this.valveType = valveType;
        this.coverExists = coverExists;
        this.position = position;
        this.meterTypeBox = meterTypeBox;
        this.pipeAfterMeter = pipeAfterMeter;
        this.insideOutside = insideOutside;
        this.pipeMaterial = pipeMaterial;
        this.pipeSize = pipeSize;
        this.sewerConnection = sewerConnection;
        this.numberOfElectricMeters = numberOfElectricMeters;
        this.electricMeterNumberLowest = electricMeterNumberLowest;
        this.postalCode = postalCode;
        this.numberOfFloors = numberOfFloors;
        BuildingCategories = buildingCategories;
        this.commercial = commercial;
        this.residential = residential;
        this.government = government;
        this.district = district;
        this.waselNumber = waselNumber;
        this.amanaBarcode = amanaBarcode;
        this.parcelID = parcelID;
        this.oldAccountNumber = oldAccountNumber;
        this.partnerNumber = partnerNumber;
        this.numberOfWaterConnections = numberOfWaterConnections;
        this.numberOfUnits = numberOfUnits;
        this.zoneCode = zoneCode;
        this.location = location;
        this.photos = photos;
    }


    public Data(int id, String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, String zoneCode, String location, Map<String, String> photos) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.houseConnNumber = houseConnNumber;
        this.amrMeterBarcode = amrMeterBarcode;
        this.waterMeterNumber = waterMeterNumber;
        this.waterMeterReading = waterMeterReading;
        this.remarks = remarks;
        this.waterMeterStatus = waterMeterStatus;
        this.meterType = meterType;
        this.meterDiameter = meterDiameter;
        this.reducerDiameter = reducerDiameter;
        this.openClose = openClose;
        this.mechElec = mechElec;
        this.valveType = valveType;
        this.coverExists = coverExists;
        this.position = position;
        this.meterTypeBox = meterTypeBox;
        this.pipeAfterMeter = pipeAfterMeter;
        this.insideOutside = insideOutside;
        this.pipeMaterial = pipeMaterial;
        this.pipeSize = pipeSize;
        this.sewerConnection = sewerConnection;
        this.numberOfElectricMeters = numberOfElectricMeters;
        this.electricMeterNumberLowest = electricMeterNumberLowest;
        this.postalCode = postalCode;
        this.numberOfFloors = numberOfFloors;
        BuildingCategories = buildingCategories;
        this.commercial = commercial;
        this.residential = residential;
        this.government = government;
        this.district = district;
        this.waselNumber = waselNumber;
        this.amanaBarcode = amanaBarcode;
        this.parcelID = parcelID;
        this.oldAccountNumber = oldAccountNumber;
        this.partnerNumber = partnerNumber;
        this.numberOfWaterConnections = numberOfWaterConnections;
        this.numberOfUnits = numberOfUnits;
        this.location = location;
        this.zoneCode = zoneCode;
        this.photos = photos;
    }

    public Data(int id, String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, String zoneCode, String location, String reason, Map<String, String> photos, String fake) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.houseConnNumber = houseConnNumber;
        this.amrMeterBarcode = amrMeterBarcode;
        this.waterMeterNumber = waterMeterNumber;
        this.waterMeterReading = waterMeterReading;
        this.remarks = remarks;
        this.waterMeterStatus = waterMeterStatus;
        this.meterType = meterType;
        this.meterDiameter = meterDiameter;
        this.reducerDiameter = reducerDiameter;
        this.openClose = openClose;
        this.mechElec = mechElec;
        this.valveType = valveType;
        this.coverExists = coverExists;
        this.position = position;
        this.meterTypeBox = meterTypeBox;
        this.pipeAfterMeter = pipeAfterMeter;
        this.insideOutside = insideOutside;
        this.pipeMaterial = pipeMaterial;
        this.pipeSize = pipeSize;
        this.sewerConnection = sewerConnection;
        this.numberOfElectricMeters = numberOfElectricMeters;
        this.electricMeterNumberLowest = electricMeterNumberLowest;
        this.postalCode = postalCode;
        this.numberOfFloors = numberOfFloors;
        BuildingCategories = buildingCategories;
        this.commercial = commercial;
        this.residential = residential;
        this.government = government;
        this.district = district;
        this.waselNumber = waselNumber;
        this.amanaBarcode = amanaBarcode;
        this.parcelID = parcelID;
        this.oldAccountNumber = oldAccountNumber;
        this.partnerNumber = partnerNumber;
        this.numberOfWaterConnections = numberOfWaterConnections;
        this.numberOfUnits = numberOfUnits;
        this.zoneCode = zoneCode;
        this.location = location;
        this.reason = reason;
        this.photos = photos;
    }

    public Data(int id, String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, String zoneCode, String location, String districtName, Map<String, String> photos) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.houseConnNumber = houseConnNumber;
        this.amrMeterBarcode = amrMeterBarcode;
        this.waterMeterNumber = waterMeterNumber;
        this.waterMeterReading = waterMeterReading;
        this.remarks = remarks;
        this.waterMeterStatus = waterMeterStatus;
        this.meterType = meterType;
        this.meterDiameter = meterDiameter;
        this.reducerDiameter = reducerDiameter;
        this.openClose = openClose;
        this.mechElec = mechElec;
        this.valveType = valveType;
        this.coverExists = coverExists;
        this.position = position;
        this.meterTypeBox = meterTypeBox;
        this.pipeAfterMeter = pipeAfterMeter;
        this.insideOutside = insideOutside;
        this.pipeMaterial = pipeMaterial;
        this.pipeSize = pipeSize;
        this.sewerConnection = sewerConnection;
        this.numberOfElectricMeters = numberOfElectricMeters;
        this.electricMeterNumberLowest = electricMeterNumberLowest;
        this.postalCode = postalCode;
        this.numberOfFloors = numberOfFloors;
        BuildingCategories = buildingCategories;
        this.commercial = commercial;
        this.residential = residential;
        this.government = government;
        this.district = district;
        this.waselNumber = waselNumber;
        this.amanaBarcode = amanaBarcode;
        this.parcelID = parcelID;
        this.oldAccountNumber = oldAccountNumber;
        this.partnerNumber = partnerNumber;
        this.numberOfWaterConnections = numberOfWaterConnections;
        this.numberOfUnits = numberOfUnits;
        this.zoneCode = zoneCode;
        this.location = location;
        this.districtName = districtName;
        this.photos = photos;

    }

    public int getId() {
        return id;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getHouseConnNumber() {
        return houseConnNumber;
    }

    public String getAmrMeterBarcode() {
        return amrMeterBarcode;
    }

    public String getWaterMeterNumber() {
        return waterMeterNumber;
    }

    public String getWaterMeterReading() {
        return waterMeterReading;
    }

    public int getRemarks() {
        return remarks;
    }

    public int getWaterMeterStatus() {
        return waterMeterStatus;
    }

    public int getMeterType() {
        return meterType;
    }

    public int getMeterDiameter() {
        return meterDiameter;
    }

    public int getReducerDiameter() {
        return reducerDiameter;
    }

    public int getOpenClose() {
        return openClose;
    }

    public int getMechElec() {
        return mechElec;
    }

    public int getValveType() {
        return valveType;
    }

    public int getCoverExists() {
        return coverExists;
    }

    public int getPosition() {
        return position;
    }

    public int getMeterTypeBox() {
        return meterTypeBox;
    }

    public int getPipeAfterMeter() {
        return pipeAfterMeter;
    }

    public int getInsideOutside() {
        return insideOutside;
    }

    public int getPipeMaterial() {
        return pipeMaterial;
    }

    public int getPipeSize() {
        return pipeSize;
    }

    public int getSewerConnection() {
        return sewerConnection;
    }

    public String getNumberOfElectricMeters() {
        return numberOfElectricMeters;
    }

    public String getElectricMeterNumberLowest() {
        return electricMeterNumberLowest;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getNumberOfFloors() {
        return numberOfFloors;
    }

    public int getBuildingCategories() {
        return BuildingCategories;
    }

    public int getCommercial() {
        return commercial;
    }

    public int getResidential() {
        return residential;
    }

    public int getGovernment() {
        return government;
    }

    public int getDistrict() {
        return district;
    }

    public String getWaselNumber() {
        return waselNumber;
    }

    public String getAmanaBarcode() {
        return amanaBarcode;
    }

    public String getParcelID() {
        return parcelID;
    }

    public String getOldAccountNumber() {
        return oldAccountNumber;
    }

    public String getPartnerNumber() {
        return partnerNumber;
    }

    public String getNumberOfWaterConnections() {
        return numberOfWaterConnections;
    }

    public String getNumberOfUnits() {
        return numberOfUnits;
    }

    public Map<String, String> getPhotos() {
        return photos;
    }

    public String getLocation() {
        return location;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReason() {
        return reason;
    }

    public void setCommercial(int commercial) {
        this.commercial = commercial;
    }

    public void setResidential(int residential) {
        this.residential = residential;
    }

    public void setGovernment(int government) {
        this.government = government;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getZoneCode() {
        return zoneCode;
    }
}
