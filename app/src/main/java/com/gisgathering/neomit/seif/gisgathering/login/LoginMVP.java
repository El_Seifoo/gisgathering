package com.gisgathering.neomit.seif.gisgathering.login;

import android.content.Context;

/**
 * Created by sief on 7/16/2018.
 */

public interface LoginMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void goHome();

    }

    interface Presenter {
        void checkIfUserLoggedIn(boolean isLoggedIn);

        void requestLogin(Context context, String username, String password, String ip);
    }
}
