package com.gisgathering.neomit.seif.gisgathering.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.gisgathering.neomit.seif.gisgathering.utils.URLs;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by sief on 7/30/2018.
 */

class OfflineModel {
    protected void getOfflineData(Context context, DBCallback callback, boolean isSyncing) {
        callback.onResponse(context, DataDbHelper.getmInstance(context).getAllData(), isSyncing);
    }

    protected void removeOneItem(Context context, DBCallback callback, int id, boolean all) {
        DataDbHelper.getmInstance(context).removeById(id);
        callback.onRemovingSuccess(id, all);
    }

    protected void syncData(final Context context, final VolleyCallback callback, final ArrayList<Data> data) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, data.get(0), true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFailed(context, error, true);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("Json[user_id]", MySingleton.getmInstance(context).getUserToken());
                params.put("Json[data][0][PlateNo]", data.get(0).getPlateNumber());
                params.put("Json[data][0][HCN]", data.get(0).getHouseConnNumber().isEmpty() ? "-1" : data.get(0).getHouseConnNumber());
                params.put("Json[data][0][AmrMeterBarcode]", data.get(0).getAmrMeterBarcode().isEmpty() ? "-1" : data.get(0).getAmrMeterBarcode());
                params.put("Json[data][0][WaterMeterNumber]", data.get(0).getWaterMeterNumber().isEmpty() ? "-1" : data.get(0).getWaterMeterNumber());
                params.put("Json[data][0][WaterMeterReading]", data.get(0).getWaterMeterReading().isEmpty() ? "-1" : data.get(0).getWaterMeterReading());
                params.put("Json[data][0][RemarkID]", data.get(0).getRemarks() + "");
                params.put("Json[data][0][StatusID]", data.get(0).getWaterMeterStatus() + "");
                params.put("Json[data][0][MeterTypeID]", data.get(0).getMeterType() + "");
                params.put("Json[data][0][MeterDiameterID]", data.get(0).getMeterDiameter() + "");
                params.put("Json[data][0][ReducerDiameterID]", data.get(0).getReducerDiameter() + "");
                params.put("Json[data][0][OpenCloseID]", data.get(0).getOpenClose() + "");
                params.put("Json[data][0][MechElecID]", data.get(0).getMechElec() + "");
                params.put("Json[data][0][ValveTypeID]", data.get(0).getValveType() + "");
                params.put("Json[data][0][CoverExists]", data.get(0).getCoverExists() + "");
                params.put("Json[data][0][Position]", data.get(0).getPosition() + "");
                params.put("Json[data][0][MeterBoxTypeID]", data.get(0).getMeterTypeBox() + "");
                params.put("Json[data][0][PipeAfterMeterID]", data.get(0).getPipeAfterMeter() + "");
                params.put("Json[data][0][MeterInOutID]", data.get(0).getInsideOutside() + "");
                params.put("Json[data][0][PipeMaterialID]", data.get(0).getPipeMaterial() + "");
                params.put("Json[data][0][PipeSizeID]", data.get(0).getPipeSize() + "");
                params.put("Json[data][0][SewerConnExists]", data.get(0).getSewerConnection() + "");
                params.put("Json[data][0][NumberOfElectricMeters]", data.get(0).getNumberOfElectricMeters().isEmpty() ? "-1" : data.get(0).getNumberOfElectricMeters());
                params.put("Json[data][0][ElectricNumberLowest]", data.get(0).getElectricMeterNumberLowest().isEmpty() ? "-1" : data.get(0).getElectricMeterNumberLowest());
                params.put("Json[data][0][PostalCode]", data.get(0).getPostalCode().isEmpty() ? "-1" : data.get(0).getPostalCode());
                params.put("Json[data][0][FloorCount]", data.get(0).getNumberOfFloors().isEmpty() ? "-1" : data.get(0).getNumberOfFloors());
                params.put("Json[data][0][PremiseID]", data.get(0).getBuildingCategories() == 5 ? "6" : data.get(0).getBuildingCategories() + "");
                params.put("Json[data][0][SubBuildingCommercialID]", data.get(0).getCommercial() + "");
                params.put("Json[data][0][SubBuildingResidentialID]", data.get(0).getResidential() + "");
                params.put("Json[data][0][SubBuildingGovernmentID]", data.get(0).getGovernment() + "");
                params.put("Json[data][0][DistrictID]", data.get(0).getDistrict() + "");
                params.put("Json[data][0][WaselNumber]", data.get(0).getWaselNumber().isEmpty() ? "-1" : data.get(0).getWaselNumber());
                params.put("Json[data][0][AmanaBarcode]", data.get(0).getAmanaBarcode().isEmpty() ? "-1" : data.get(0).getAmanaBarcode());
                params.put("Json[data][0][Parcel_ID]", data.get(0).getParcelID().isEmpty() ? "-1" : data.get(0).getParcelID());
                params.put("Json[data][0][Old_Sec_Account]", data.get(0).getOldAccountNumber().isEmpty() ? "-1" : data.get(0).getOldAccountNumber());
                params.put("Json[data][0][partner_number]", data.get(0).getPartnerNumber().isEmpty() ? "-1" : data.get(0).getPartnerNumber());
                params.put("Json[data][0][Number_of_Water_Connections]", data.get(0).getNumberOfWaterConnections().isEmpty() ? "-1" : data.get(0).getNumberOfWaterConnections());
                params.put("Json[data][0][Number_of_Units]", data.get(0).getNumberOfUnits().isEmpty() ? "-1" : data.get(0).getNumberOfUnits());
                params.put("Json[data][0][XCoordinate]", !data.get(0).getLocation().isEmpty() ? data.get(0).getLocation().split(",")[0] : "-1");
                params.put("Json[data][0][YCoordinate]", !data.get(0).getLocation().isEmpty() ? data.get(0).getLocation().split(",")[1] : "-1");
                params.put("Json[data][0][ZoneCode]", !data.get(0).getZoneCode().isEmpty() ? data.get(0).getZoneCode() : "-1");
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void syncRecord(final Context context, final VolleyCallback callback, final Data data) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, data, false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFailed(context, error, false);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken());
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("Json[data][0][PlateNo]", data.getPlateNumber());
                params.put("Json[data][0][HCN]", data.getHouseConnNumber().isEmpty() ? "-1" : data.getHouseConnNumber());
                params.put("Json[data][0][AmrMeterBarcode]", data.getAmrMeterBarcode().isEmpty() ? "-1" : data.getAmrMeterBarcode());
                params.put("Json[data][0][WaterMeterNumber]", data.getWaterMeterNumber().isEmpty() ? "-1" : data.getWaterMeterNumber());
                params.put("Json[data][0][WaterMeterReading]", data.getWaterMeterReading().isEmpty() ? "-1" : data.getWaterMeterReading());
                params.put("Json[data][0][RemarkID]", data.getRemarks() + "");
                params.put("Json[data][0][StatusID]", data.getWaterMeterStatus() + "");
                params.put("Json[data][0][MeterTypeID]", data.getMeterType() + "");
                params.put("Json[data][0][MeterDiameterID]", data.getMeterDiameter() + "");
                params.put("Json[data][0][ReducerDiameterID]", data.getReducerDiameter() + "");
                params.put("Json[data][0][OpenCloseID]", data.getOpenClose() + "");
                params.put("Json[data][0][MechElecID]", data.getMechElec() + "");
                params.put("Json[data][0][ValveTypeID]", data.getValveType() + "");
                params.put("Json[data][0][CoverExists]", data.getCoverExists() + "");
                params.put("Json[data][0][Position]", data.getPosition() + "");
                params.put("Json[data][0][MeterBoxTypeID]", data.getMeterTypeBox() + "");
                params.put("Json[data][0][PipeAfterMeterID]", data.getPipeAfterMeter() + "");
                params.put("Json[data][0][MeterInOutID]", data.getInsideOutside() + "");
                params.put("Json[data][0][PipeMaterialID]", data.getPipeMaterial() + "");
                params.put("Json[data][0][PipeSizeID]", data.getPipeSize() + "");
                params.put("Json[data][0][SewerConnExists]", data.getSewerConnection() + "");
                params.put("Json[data][0][NumberOfElectricMeters]", data.getNumberOfElectricMeters().isEmpty() ? "-1" : data.getNumberOfElectricMeters());
                params.put("Json[data][0][ElectricNumberLowest]", data.getElectricMeterNumberLowest().isEmpty() ? "-1" : data.getElectricMeterNumberLowest());
                params.put("Json[data][0][PostalCode]", data.getPostalCode().isEmpty() ? "-1" : data.getPostalCode());
                params.put("Json[data][0][FloorCount]", data.getNumberOfFloors().isEmpty() ? "-1" : data.getNumberOfFloors());
                params.put("Json[data][0][PremiseID]", data.getBuildingCategories() == 5 ? "6" : data.getBuildingCategories() + "");
                params.put("Json[data][0][SubBuildingCommercialID]", data.getCommercial() + "");
                params.put("Json[data][0][SubBuildingResidentialID]", data.getResidential() + "");
                params.put("Json[data][0][SubBuildingGovernmentID]", data.getGovernment() + "");
                params.put("Json[data][0][DistrictID]", data.getDistrict() + "");
                params.put("Json[data][0][WaselNumber]", data.getWaselNumber().isEmpty() ? "-1" : data.getWaselNumber());
                params.put("Json[data][0][AmanaBarcode]", data.getAmanaBarcode().isEmpty() ? "-1" : data.getAmanaBarcode());
                params.put("Json[data][0][Parcel_ID]", data.getParcelID().isEmpty() ? "-1" : data.getParcelID());
                params.put("Json[data][0][Old_Sec_Account]", data.getOldAccountNumber().isEmpty() ? "-1" : data.getOldAccountNumber());
                params.put("Json[data][0][partner_number]", data.getPartnerNumber().isEmpty() ? "-1" : data.getPartnerNumber());
                params.put("Json[data][0][Number_of_Water_Connections]", data.getNumberOfWaterConnections().isEmpty() ? "-1" : data.getNumberOfWaterConnections());
                params.put("Json[data][0][Number_of_Units]", data.getNumberOfUnits().isEmpty() ? "-1" : data.getNumberOfUnits());
                params.put("Json[data][0][XCoordinate]", !data.getLocation().isEmpty() ? data.getLocation().split(",")[0] : "-1");
                params.put("Json[data][0][YCoordinate]", !data.getLocation().isEmpty() ? data.getLocation().split(",")[1] : "-1");
                params.put("Json[data][0][ZoneCode]", !data.getZoneCode().isEmpty() ? data.getZoneCode() : "-1");
                Log.e("param", params.toString());
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void syncPics(final Context context, final UploadingPhotoCallback callback, final int dataId, Map<String, String> photos, String plateNumber, final boolean all) {
        try {
            UploadService.NAMESPACE = "com.gisgathering.neomit.seif.gisgathering";
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, UUID.randomUUID().toString(), URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.SYNC_PHOTOS);

            final ArrayList<String> paths = new ArrayList<>();
            if (photos.get("meterConnectionFullView") != null && photos.get("meterConnectionFullView").contains("//////")) {
                Log.e("meterConnectionFullView", photos.get("meterConnectionFullView").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("meterConnectionFullView").split("//////")[1], plateNumber + "_full");
                paths.add(photos.get("meterConnectionFullView").split("//////")[1]);

            }
            if (photos.get("meterCloseUpView") != null && photos.get("meterCloseUpView").contains("//////")) {
                Log.e("meterCloseUpView", photos.get("meterCloseUpView").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("meterCloseUpView").split("//////")[1], plateNumber + "_close");
                paths.add(photos.get("meterCloseUpView").split("//////")[1]);
            }
            if (photos.get("electricMeterPhoto") != null && photos.get("electricMeterPhoto").contains("//////")) {
                Log.e("electricMeterPhoto", photos.get("electricMeterPhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("electricMeterPhoto").split("//////")[1], plateNumber + "_elec");
                paths.add(photos.get("electricMeterPhoto").split("//////")[1]);
            }
            if (photos.get("propertyPhoto") != null && photos.get("propertyPhoto").contains("//////")) {
                Log.e("propertyPhoto", photos.get("propertyPhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("propertyPhoto").split("//////")[1], plateNumber + "_property");
                paths.add(photos.get("propertyPhoto").split("//////")[1]);
            }
            if (photos.get("amanaPlatePhoto") != null && photos.get("amanaPlatePhoto").contains("//////")) {
                Log.e("amanaPlatePhoto", photos.get("amanaPlatePhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("amanaPlatePhoto").split("//////")[1], plateNumber + "_amana");
                paths.add(photos.get("amanaPlatePhoto").split("//////")[1]);
            }
            if (photos.get("NWCPlatePhoto") != null && photos.get("NWCPlatePhoto").contains("//////")) {
                Log.e("NWCPlatePhoto", photos.get("NWCPlatePhoto").split("//////")[1]);
                multipartUploadRequest.addFileToUpload(photos.get("NWCPlatePhoto").split("//////")[1], plateNumber + "_nwc");
                paths.add(photos.get("NWCPlatePhoto").split("//////")[1]);
            }

            multipartUploadRequest
//                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Authorization", "Bearer " + MySingleton.getmInstance(context).getUserToken())
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(0)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {

                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
                            Log.e("error", exception.toString());
                        }


                        @Override
                        public void onCompleted(Context context1, UploadInfo uploadInfo, ServerResponse serverResponse) {
                            try {
//                                JSONObject response = new JSONObject(new String(serverResponse.getBody(), "UTF-8"));
                                JSONArray array = new JSONArray(new String(serverResponse.getBody(), "UTF-8"));
                                Log.e("server response", array.toString());
                                callback.onUploadSuccess(context, array, dataId, all, paths);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record), all);
                        }
                    })
                    .startUpload();
        } catch (Exception e) {
            Log.e("e upload", e.toString());
            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record), all);
        }
    }

    protected interface DBCallback {
        void onResponse(Context context, ArrayList<Data> dataList, boolean isSyncing);

        void onRemovingSuccess(int id, boolean all);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String responseString, Data data, boolean all) throws JSONException;

        void onFailed(Context context, VolleyError error, boolean all);
    }

    protected interface UploadingPhotoCallback {
        void onUploadSuccess(Context context, JSONArray response, int dataId, boolean all, ArrayList<String> paths) throws JSONException;

        void onUploadFailed(String message, boolean all);

        void noPicsToUpload(String message, boolean all);
    }

}


/*
Json[data][0][PlateNo]:00024
Json[data][0][HCN]:150285777
Json[data][0][AmrMeterBarcode]:1
Json[data][0][WaterMeterNumber]:1
Json[data][0][WaterMeterReading]:1
Json[data][0][RemarkID]:1
Json[data][0][StatusID]:1
Json[data][0][MeterTypeID]:1
Json[data][0][MeterDiameterID]:1
Json[data][0][ReducerDiameterID]:1
Json[data][0][OpenCloseID]:1
Json[data][0][MechElecID]:1
Json[data][0][ValveTypeID]:1
Json[data][0][CoverExists]:1
Json[data][0][Position]:1
Json[data][0][MeterBoxTypeID]:1
Json[data][0][PipeAfterMeterID]:1
Json[data][0][MeterInOutID]:1
Json[data][0][PipeMaterialID]:1
Json[data][0][PipeSizeID]:1
Json[data][0][SewerConnExists]:1
Json[data][0][NumberOfElectricMeters]:1
Json[data][0][ElectricNumberLowest]:1
Json[data][0][PostalCode]:1
Json[data][0][FloorCount]:1
Json[data][0][PremiseID]:1
Json[data][0][SubBuildingCommercialID]:2
Json[data][0][SubBuildingResidentialID]:1
Json[data][0][SubBuildingGovernmentID]:1
Json[data][0][DistrictID]:4
Json[data][0][WaselNumber]:1
Json[data][0][AmanaBarcode]:1
Json[data][0][Parcel_ID]:1
Json[data][0][Old_Sec_Account]:1
Json[data][0][partner_number]:1
Json[data][0][Number_of_Water_Connections]:1
Json[data][0][Number_of_Units]:1
Json[data][0][XCoordinate]:1
Json[data][0][YCoordinate]:1
 */
