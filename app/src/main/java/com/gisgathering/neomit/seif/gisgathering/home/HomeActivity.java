package com.gisgathering.neomit.seif.gisgathering.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataCollectionActivity;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.infoMap.InfoMapActivity;
import com.gisgathering.neomit.seif.gisgathering.login.LoginActivity;
import com.gisgathering.neomit.seif.gisgathering.offline.OfflineActivity;
import com.gisgathering.neomit.seif.gisgathering.rejected.RejectedActivity;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String localLanguage = MySingleton.getmInstance(getApplicationContext()).getAppLang();
        if (localLanguage.equals(getString(R.string.settings_language_arabic_value))) {
            setLocale("ar", this);
        } else if (localLanguage.equals(getString(R.string.settings_language_english_value))) {
            setLocale("en", this);
        }

        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle(getString(R.string.home));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_button:
                Intent intent1 = new Intent(getApplicationContext(), DataCollectionActivity.class);
                intent1.putExtra("Add", "Add");
                startActivity(intent1);
                break;
            case R.id.offline_button:
                startActivity(new Intent(getApplicationContext(), OfflineActivity.class));
                break;
            case R.id.get_info_button:
                Intent intent = new Intent(getApplicationContext(), InfoMapActivity.class);
                intent.putExtra("GetInfo", "GetInfo");
                startActivity(intent);
                break;
            case R.id.map_button:
                startActivity(new Intent(getApplicationContext(), InfoMapActivity.class));
                break;
            case R.id.reject_button:
                startActivity(new Intent(getApplicationContext(), RejectedActivity.class));
                break;
            case R.id.logout_button:
                String ip = MySingleton.getmInstance(this).getUserIP();
                MySingleton.getmInstance(this).logout();
                DataDbHelper.getmInstance(this).deleteDistricts();
                MySingleton.getmInstance(this).saveUserIp(ip);
                Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.language:
                if (MySingleton.getmInstance(getApplicationContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    MySingleton.getmInstance(getApplicationContext()).setAppLang(getString(R.string.settings_language_english_value));
                } else if (MySingleton.getmInstance(getApplicationContext()).getAppLang().equals(getString(R.string.settings_language_english_value))) {
                    MySingleton.getmInstance(getApplicationContext()).setAppLang(getString(R.string.settings_language_arabic_value));
                }
                finish();
                startActivity(getIntent());
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    /*
     *  Change the configuration of the Application depending on App Language
     */
    private void setLocale(String lang, Context context) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            getApplicationContext().getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }
}
