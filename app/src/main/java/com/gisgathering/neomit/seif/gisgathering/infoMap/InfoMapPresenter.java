package com.gisgathering.neomit.seif.gisgathering.infoMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sief on 8/8/2018.
 */

public class InfoMapPresenter implements InfoMapMVP.Presenter, InfoMapModel.VolleyCallback {
    private InfoMapMVP.View view;
    private InfoMapModel model;

    public InfoMapPresenter(InfoMapMVP.View view, InfoMapModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestInfoMap(Context context, String plateNumber, boolean isRequestingInfo) {
        view.showProgress();
        if (isRequestingInfo) model.getInfo(context, this, plateNumber);
        else model.locate(context, this, plateNumber);
    }

    @Override
    public void requestQRScanner(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.handleQRScannerResult(result.getContents());
        }
    }

    private int returnValidInt(String data) {
        return data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty() ? -1 : Integer.valueOf(data);
    }

    private String returnValidString(String data) {
        return data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty() || data.equals("-1") ? "" : data;
    }

    @Override
    public void onSuccess(Context context, String response, int step) throws JSONException {
        view.hideProgress();
        JSONObject jsonObject = new JSONObject(response);
        if (step == 0) {
            if (jsonObject.has("status"))
                view.showToastMessage(context.getString(R.string.this_meter_has_no_info));
            else {
                JSONArray data = jsonObject.getJSONArray("data");
                Map<String, String> photos = new LinkedHashMap<>();
                photos.put("meterConnectionFullView", data.getJSONObject(0).getString("full_img"));
                photos.put("meterCloseUpView", data.getJSONObject(0).getString("close_img"));
                photos.put("electricMeterPhoto", data.getJSONObject(0).getString("elec_img"));
                photos.put("propertyPhoto", data.getJSONObject(0).getString("property_img"));
                photos.put("amanaPlatePhoto", data.getJSONObject(0).getString("amana_img"));
                photos.put("NWCPlatePhoto", data.getJSONObject(0).getString("nwc_img"));
                int buildingCat = returnValidInt(data.getJSONObject(0).getString("BuildingArea"));
                view.showInfo(new Data(data.getJSONObject(0).getInt("ID"), returnValidString(data.getJSONObject(0).getString("PlateNo")),
                        returnValidString(data.getJSONObject(0).getString("HCN")), returnValidString(data.getJSONObject(0).getString("AmrMeterBarcode")),
                        returnValidString(data.getJSONObject(0).getString("WaterMeterNumber")), returnValidString(data.getJSONObject(0).getString("WaterMeterReading")),
                        returnValidInt(data.getJSONObject(0).getString("RemarkID")), returnValidInt(data.getJSONObject(0).getString("StatusID")),
                        returnValidInt(data.getJSONObject(0).getString("MeterTypeID")), returnValidInt(data.getJSONObject(0).getString("MeterDiameterID")),
                        returnValidInt(data.getJSONObject(0).getString("ReducerDiameterID")), returnValidInt(data.getJSONObject(0).getString("OpenCloseID")),
                        returnValidInt(data.getJSONObject(0).getString("MechElecID")), returnValidInt(data.getJSONObject(0).getString("ValveTypeID")),
                        returnValidInt(data.getJSONObject(0).getString("CoverExists")), returnValidInt(data.getJSONObject(0).getString("position")),
                        returnValidInt(data.getJSONObject(0).getString("MeterBoxTypeID")), returnValidInt(data.getJSONObject(0).getString("PipeAfterMeterID")),
                        returnValidInt(data.getJSONObject(0).getString("MeterInOutID")), returnValidInt(data.getJSONObject(0).getString("PipeMaterialID")),
                        returnValidInt(data.getJSONObject(0).getString("PipeSizeID")), returnValidInt(data.getJSONObject(0).getString("SewerConnExists")),
                        returnValidString(data.getJSONObject(0).getString("NumberOfElectricMeters")), returnValidString(data.getJSONObject(0).getString("ElectricNumberLowest")),
                        returnValidString(data.getJSONObject(0).getString("PostalCode")), returnValidString(data.getJSONObject(0).getString("FloorCount")),
                        buildingCat == 6 ? 5 : buildingCat, returnValidInt(data.getJSONObject(0).getString("SubBuildingCommercialID")),
                        returnValidInt(data.getJSONObject(0).getString("SubBuildingResidentialID")), returnValidInt(data.getJSONObject(0).getString("SubBuildingGovernmentID")),
                        returnValidInt(data.getJSONObject(0).getString("DistrictID")), returnValidString(data.getJSONObject(0).getString("WaselNumber")),
                        returnValidString(data.getJSONObject(0).getString("AmanaBarcode")), returnValidString(data.getJSONObject(0).getString("Parcel_ID")),
                        returnValidString(data.getJSONObject(0).getString("Old_Sec_Account")), returnValidString(data.getJSONObject(0).getString("partner_number")),
                        returnValidString(data.getJSONObject(0).getString("Number_of_Water_Connections")), returnValidString(data.getJSONObject(0).getString("Number_of_Units")), returnValidString(data.getJSONObject(0).getString("ZoneCode")),
                        returnValidString(data.getJSONObject(0).getString("XCoordinate")) + "," + returnValidString(data.getJSONObject(0).getString("YCoordinate")), photos))
                ;
            }
        } else {
            if (jsonObject.has("status"))
                view.showToastMessage(context.getString(R.string.this_meter_has_no_location_data));
            else
                view.showLocation(jsonObject.getDouble("longitude"), jsonObject.getDouble("latitude"));
        }
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        if (error instanceof AuthFailureError) {
            view.showToastMessage(context.getString(R.string.your_session_ended_please_relogin));
            view.goLogin();
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
