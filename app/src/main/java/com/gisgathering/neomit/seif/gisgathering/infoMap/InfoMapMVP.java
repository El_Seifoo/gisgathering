package com.gisgathering.neomit.seif.gisgathering.infoMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.google.zxing.integration.android.IntentIntegrator;

/**
 * Created by sief on 8/8/2018.
 */

public interface InfoMapMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showLocation(double longitude, double latitude);

        void initializeScanner(IntentIntegrator integrator);

        void handleQRScannerResult(String result);

        void showInfo(Data data);

        void goLogin();
    }

    interface Presenter {
        void requestInfoMap(Context context, String plateNumber, boolean isRequestingInfo);

        void requestQRScanner(Activity activity);

        void onActivityResult(Context context, int requestCode, int resultCode, Intent data);
    }
}
