package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.provider.BaseColumns;

/**
 * Created by sief on 7/29/2018.
 */

public class DataContract {
    public class DataEntry implements BaseColumns {
        public static final String TABLE_NAME = "data";
        public static final String ID = "id";
        public static final String PLATE_NUMBER = "plateNumber";
        public static final String HOUSE_CONNECTION_NUMBER = "houseConnNumber";
        public static final String AMR_METER_BARCODE = "amrMeterBarcode";
        public static final String WATER_METER_NUMBER = "waterMeterNumber";
        public static final String WATER_METER_READING = "waterMeterReading";
        public static final String REMARKS = "remarks";
        public static final String WATER_METER_STATUS = "waterMeterStatus";
        public static final String METER_TYPE = "meterType";
        public static final String METER_DIAMETER = "meterDiameter";
        public static final String REDUCER_DIAMETER = "reducerDiameter";
        public static final String OPEN_CLOSE = "openClose";
        public static final String MECH_ELEC = "mechElec";
        public static final String VALVE_TYPE = "valveType";
        public static final String COVER_EXISTS = "coverExists";
        public static final String POSITION = "position";
        public static final String METER_TYPE_BOX = "meterTypeBox";
        public static final String PIPE_AFTER_METER = "pipeAfterMeter";
        public static final String INSIDE_OUTSIDE = "insideOutside";
        public static final String PIPE_MATERIAL = "pipeMaterial";
        public static final String PIPE_SIZE = "pipeSize";
        public static final String SEWER_CONNECTION = "sewerConnection";
        public static final String NUMBER_OF_ELECTRIC_METERS = "numberOfElectricMeters";
        public static final String ELECTRIC_METER_NUMBER_LOWEST = "electricMeterNumberLowest";
        public static final String POSTAL_CODE = "postalCode";
        public static final String NUMBER_OF_FLOORS = "numberOfFloors";
        public static final String BUILDING_CATEGORY = "BuildingCategory";
        public static final String SUB_BUILDING_COMMERCIAL = "commercial";
        public static final String SUB_BUILDING_RESIDENTIAL = "residential";
        public static final String SUB_BUILDING_GOVERNMENT = "government";
        public static final String DISTRICT = "district";
        public static final String DISTRICT_NAME = "districtName";
        public static final String WASEL_NUMBER = "waselNumber";
        public static final String AMANA_BARCODE = "amanaBarcode";
        public static final String PARCEL_ID = "parcelID";
        public static final String OLD_ACCOUNT_NUMBER = "oldAccountNumber";
        public static final String PARTENER_NUMBER = "partnerNumber";
        public static final String NUMBER_OF_WATER_CONNECTIONS = "numberOfWaterConnections";
        public static final String NUMBER_OF_UNITS = "numberOfUnits";
        public static final String ZONE_CODE = "zoneCode";
        public static final String METER_CONNECTION_FULL_VIEW_IMG = "meterConnectionFullView";
        public static final String METER_CLOSE_UP_VIEW_IMG = "meterCloseUpView";
        public static final String ELECTRIC_METER_IMG = "electricMeterPhoto";
        public static final String PROPERTY_IMG = "propertyPhoto";
        public static final String AMANA_PLATE_IMG = "amanaPlatePhoto";
        public static final String NWC_PLATE_IMG = "NWCPlatePhoto";
        public static final String LOCATION = "location";


        public static final String TABLE_NAME_1 = "districts";
        public static final String DISTRICT_ID = "id";
        public static final String DISTRICT_NAME_1 = "district";
        public static final String DISTRICT_ID_1 = "district_id";
    }
}
