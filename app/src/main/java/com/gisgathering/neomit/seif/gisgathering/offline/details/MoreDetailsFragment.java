package com.gisgathering.neomit.seif.gisgathering.offline.details;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public class MoreDetailsFragment extends Fragment implements MoreDetailsMVP.View {

    private RecyclerView recyclerView;
    private MoreDetailsAdapter adapter;
    private MoreDetailsMVP.Presenter presenter;

    /*
     *  Static method to initialize fragment
     *  no params
     *  return MoreDetailsFragment
     */
    public static MoreDetailsFragment newInstance(Data data) {
        MoreDetailsFragment fragment = new MoreDetailsFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("Data", data);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more_details, container, false);
        presenter = new MoreDetailsPresenter(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.more_details_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MoreDetailsAdapter();
        presenter.requestSpinnersStrings(getContext(), (Data) getArguments().getSerializable("Data"));
        return view;
    }

    @Override
    public void loadData(ArrayList<MoreDetails> moreDetailsList, boolean hasImgs, int count) {
        adapter.setMoreDetailsList(moreDetailsList, hasImgs, count);
        recyclerView.setAdapter(adapter);
    }
}
