package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;


import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gisgathering.neomit.seif.gisgathering.R;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by sief on 7/26/2018.
 */


public class DataCollectionPresenter implements DataCollectionMVP.Presenter, DataCollectionModel.DBCallback, DataCollectionModel.VolleyCallback, DataCollectionModel.UploadingPhotoCallback {
    private DataCollectionMVP.View view;
    private DataCollectionModel model;

    public DataCollectionPresenter(DataCollectionMVP.View view, DataCollectionModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void requestEditingData(Activity activity, boolean isEditing) {
        if (!isEditing) return;
        Data data = (Data) activity.getIntent().getSerializableExtra("Editing");
        String[] editTextsData = {data.getPlateNumber(), data.getAmrMeterBarcode(), data.getAmanaBarcode(), data.getHouseConnNumber(), data.getWaterMeterNumber(),
                data.getWaterMeterReading(), data.getNumberOfElectricMeters(), data.getElectricMeterNumberLowest(), data.getPostalCode(), data.getNumberOfFloors(),
                data.getWaselNumber(), data.getParcelID(), data.getOldAccountNumber(), data.getPartnerNumber(), data.getNumberOfWaterConnections(), data.getNumberOfUnits(), data.getZoneCode()};
        boolean[] flags = new boolean[editTextsData.length];
        for (int i = 0; i < editTextsData.length; i++) {
            if (editTextsData[i].isEmpty()) flags[i] = false;
            else flags[i] = true;
        }
        view.loadEditingDataEditTexts(data, editTextsData, flags);
    }

    @Override
    public void requestEditingPhotos(Map<String, String> photos, ImageView[] imageViews, boolean isSynced, boolean isRejected) {
        paths = photos;

        String[] links = {photos.get("meterConnectionFullView"), photos.get("meterCloseUpView"), photos.get("electricMeterPhoto"),
                photos.get("propertyPhoto"), photos.get("amanaPlatePhoto"), photos.get("NWCPlatePhoto")};
        for (int i = 0; i < imageViews.length; i++) {
            if (isSynced || isRejected) view.setEditingImages(imageViews[i], links[i]);
            else {
                if (links[i] != null)
                    view.setEditingImages(imageViews[i], stringToBitmap(links[i].split("//////")[0]));
            }
        }

    }

    @Override
    public void requestDistricts(Context context) {
        view.showProgress();
        model.getDistricts(context, this);
    }

    public Bitmap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public void requestAllSpinnersList(Activity context, Spinner[] spinners, String[][] allLists, boolean isEditing) {
        Data data;
        int[] ids = null;
        if (isEditing) {
            data = (Data) context.getIntent().getSerializableExtra("Editing");
            ids = new int[]{returnCorrespondingStringsOne(data.getRemarks()), returnCorrespondingStringsTwo(data.getWaterMeterStatus()), returnCorrespondingStringsOne(data.getMeterType()),
                    returnCorrespondingStringsOne(data.getMeterDiameter()), returnCorrespondingStringsOne(data.getReducerDiameter()), returnCorrespondingStringsOne(data.getOpenClose()),
                    returnCorrespondingStringsOne(data.getMechElec()), returnCorrespondingStringsOne(data.getValveType()), returnCorrespondingStringsOne(data.getCoverExists()),
                    returnCorrespondingStringsOne(data.getPosition()), returnCorrespondingStringsOne(data.getMeterTypeBox()), returnCorrespondingStringsOne(data.getPipeAfterMeter()),
                    returnCorrespondingStringsOne(data.getInsideOutside()), returnCorrespondingStringsOne(data.getPipeMaterial()), returnCorrespondingStringsOne(data.getPipeSize()),
                    returnCorrespondingStringsOne(data.getSewerConnection()), returnCorrespondingStringsOne(data.getBuildingCategories()), returnCorrespondingStringsFour(data.getCommercial()),
                    returnCorrespondingStringsOne(data.getGovernment()), returnCorrespondingStringsOne(data.getResidential())};
        }
        for (int i = 0; i < spinners.length - 1; i++) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context.getApplicationContext(), android.R.layout.simple_spinner_item, allLists[i]);

            final int finalI = i;
            spinners[i].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view1, int position, long id) {
                    if (position != 0)
                        view.clearFocus();
                    if (finalI == 16)
                        // com , res, gov
                        /*
                        <item>N/A</item>
                        <item>Royal</item>
                        <item>Commercial</item>
                        <item>Government</item>
                        <item>Residential</item>
                        <item>tanker</item>
                         */
                        view.showHideBuildingCategs(position == 2, position == 4, position == 3, position == 5);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            if (isEditing) view.setSpinnersData(spinners[i], adapter, ids[i]);
            else
                view.setSpinnersData(spinners[i], adapter);
        }
    }


    // for lists that ids -1,1,2,.....
    private int returnCorrespondingStringsOne(int id) {
        if (id == -1) return 0;
        else return id;
    }

    // for water meter status list ...
    private int returnCorrespondingStringsTwo(int id) {
        if (id == -1) return 0;
        else return id + 1;
    }

    // for lists that ids -1,2,3,4,.....
    private int returnCorrespondingStringsFour(int id) {
        if (id == -1) return 0;
        else return id - 1;
    }


    private final static int SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST = 10;
    private final static int SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST = 20;
    private final static int SELECT_ELECTRIC_METER_IMG_REQUEST = 30;
    private final static int SELECT_PROPERTY_IMG_REQUEST = 40;
    private final static int SELECT_AMANA_PLATE_IMG_REQUEST = 50;
    private final static int SELECT_NWC_PLATE_IMG_REQUEST = 60;


    @Override
    public void whichView(Activity activity, View view, boolean isPickingImage) {
        if (!isPickingImage) // qr code
            switch (view.getId()) {
                case R.id.plate_number_qr:
                    this.requestQRScanner(activity, "plateNumber");
                    break;
                case R.id.house_connection_number_qr:
                    this.requestQRScanner(activity, "houseConnectionNumber");
                    break;
                case R.id.amr_meter_barcode_qr:
                    this.requestQRScanner(activity, "amrMeterBarcode");
                    break;
                case R.id.amana_barcode_qr:
                    this.requestQRScanner(activity, "amanaBarcode");
            }
        else// pick photo
            requestPermission(activity, view.getId() == R.id.meter_connection_full_view_button ? SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST :
                    view.getId() == R.id.meter_close_up_view_button ? SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST :
                            view.getId() == R.id.electric_meter_photo_button ? SELECT_ELECTRIC_METER_IMG_REQUEST :
                                    view.getId() == R.id.property_photo_button ? SELECT_PROPERTY_IMG_REQUEST :
                                            view.getId() == R.id.amana_plate_photo_button ? SELECT_AMANA_PLATE_IMG_REQUEST :
                                                    SELECT_NWC_PLATE_IMG_REQUEST);
    }

    private void requestPermission(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(
                activity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                requestCode
        );
    }

    @Override
    public void requestQRScanner(Activity activity, String editText) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (editText.equals("plateNumber")) {
            flag = 0;
        } else if (editText.equals("houseConnectionNumber")) {
            flag = 1;
        } else if (editText.equals("amrMeterBarcode")) {
            flag = 2;
        } else if (editText.equals("amanaBarcode")) {
            flag = 3;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void handleQRScannerResult(String data, int flag, EditText[] editTexts) {
        if (flag == 0)
            view.setQRData(editTexts[flag], data.replaceAll("[^0-9]", ""));
        else
            view.setQRData(editTexts[flag], data);
    }

    @Override
    public void onRequestPermissionsResult(Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults, ImageView[] imageViews) {
        if (requestCode == SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST || requestCode == SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST || requestCode == SELECT_ELECTRIC_METER_IMG_REQUEST ||
                requestCode == SELECT_PROPERTY_IMG_REQUEST || requestCode == SELECT_AMANA_PLATE_IMG_REQUEST || requestCode == SELECT_NWC_PLATE_IMG_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent capturePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (capturePhoto.resolveActivity(activity.getApplicationContext().getPackageManager()) != null) {
                    view.startPickingPhotoForResult(capturePhoto, requestCode);
                }
            } else {
//                Toast.makeText(getApplicationContext(), "permission Denied", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSION_FINE_LOCATION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                view.showToastMessage(activity.getApplicationContext().getString(R.string.need_location_permission_message));
                activity.finish();
                return;
            }
        }
    }

    Bitmap bitmap;
    Uri filePath;
    Map<String, String> paths = new LinkedHashMap<>(6);

    @Override
    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data, ImageView[] imageViews, EditText[] editTexts) {
        Log.e("requestCode", requestCode + "");
        if (requestCode == SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST || requestCode == SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST || requestCode == SELECT_ELECTRIC_METER_IMG_REQUEST ||
                requestCode == SELECT_PROPERTY_IMG_REQUEST || requestCode == SELECT_AMANA_PLATE_IMG_REQUEST || requestCode == SELECT_NWC_PLATE_IMG_REQUEST) {
            if (resultCode == RESULT_OK && data != null) {
//                filePath = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                try {
                    filePath = createImageFile(bitmapToByte(bitmap));
                    Log.e("filepath", filePath.toString());
                    long length = bitmapToByte(bitmap).length;
                    Log.e("length", length + "");
                    if ((length / 1024) > 1024) {
                        view.showToastMessage(context.getString(R.string.cant_upload_more_than_1_mega));
                        return;
                    }
                    switch (requestCode) {
                        case SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST:
                            paths.put("meterConnectionFullView", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                            break;
                        case SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST:
                            paths.put("meterCloseUpView", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                            break;
                        case SELECT_ELECTRIC_METER_IMG_REQUEST:
                            paths.put("electricMeterPhoto", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                            break;
                        case SELECT_PROPERTY_IMG_REQUEST:
                            paths.put("propertyPhoto", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                            break;
                        case SELECT_AMANA_PLATE_IMG_REQUEST:
                            paths.put("amanaPlatePhoto", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                            break;
                        case SELECT_NWC_PLATE_IMG_REQUEST:
                            paths.put("NWCPlatePhoto", bitmapToString(bitmap) + "//////" + getPath(context, filePath));
                    }
                    view.loadSelectedPic(bitmapToByte(bitmap), requestCode == SELECT_METER_CONNECTION_FULL_VIEW_IMG_REQUEST ? imageViews[0] :
                            requestCode == SELECT_METER_CLOSE_UP_VIEW_IMG_REQUEST ? imageViews[1] :
                                    requestCode == SELECT_ELECTRIC_METER_IMG_REQUEST ? imageViews[2] :
                                            requestCode == SELECT_PROPERTY_IMG_REQUEST ? imageViews[3] :
                                                    requestCode == SELECT_AMANA_PLATE_IMG_REQUEST ? imageViews[4] :
                                                            imageViews[5]);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK && data != null) {
            Place place = PlacePicker.getPlace(context.getApplicationContext(), data);
            if (place == null) {
                view.showToastMessage(context.getString(R.string.no_place_selected));
                return;
            }
            // extract the place information ...
            String latitude = returnLocation(String.valueOf(place.getLatLng().latitude));
            String longitude = returnLocation(String.valueOf(place.getLatLng().longitude));
            Log.e("location", latitude + "," + longitude);
            view.setLocation(latitude + "," + longitude);
            return;
        }
        {
            IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
            if (result != null) {
                if (result.getContents() != null)
                    this.handleQRScannerResult(result.getContents(), requestCode, editTexts);
            }
        }
    }

    private String returnLocation(String location) {
        if (location.contains("E")) return location.split("E")[0];
        else return location;
    }

    @Override
    public void preRequestSaveData(Context context, EditText[] editTexts, Spinner[] spinners, String location, boolean isSaving, boolean isSynced) {
        //get all spinners user input ...
        spinners(context, editTexts, spinners, location, isSaving, isSynced, false);
    }

    @Override
    public void preRequestSaveData(Context context, EditText[] editTexts, Spinner[] spinners, String location) {
        spinners(context, editTexts, spinners, location, false, false, true);
    }

    private void spinners(Context context, EditText[] editTexts, Spinner[] spinners, String location, boolean isSaving, boolean isSynced, boolean isRejected) {
        int[] spinnersData = new int[spinners.length];
        int selectedItemPosition;
        for (int i = 0; i < spinners.length; i++) {
            selectedItemPosition = spinners[i].getSelectedItemPosition();
            if (i == 0 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 9 || i == 10 || i == 11
                    || i == 13 || i == 14 || i == 18 || i == 19 || i == 8 || i == 12 || i == 16 || i == 15) {
                if (selectedItemPosition == 0) spinnersData[i] = -1;
                else spinnersData[i] = selectedItemPosition;
            } else if (i == 1) {
                if (selectedItemPosition == 0) spinnersData[i] = -1;
                else spinnersData[i] = selectedItemPosition - 1;
            } else if (i == 17) {
                if (selectedItemPosition == 0) spinnersData[i] = -1;
                else spinnersData[i] = selectedItemPosition + 1;
            }
        }

        // get all editTexts user input ...
        editTexts(context, editTexts, spinnersData, location, isSaving, isSynced, isRejected);
    }

    private void editTexts(Context context, EditText[] editTexts, int[] spinnersData, String location, boolean isSaving, boolean isSynced, boolean isRejected) {
        String[] editTextsData = new String[editTexts.length];
        for (int i = 0; i < editTexts.length; i++) {
            editTextsData[i] = editTexts[i].getText().toString().trim();
        }

        if (editTextsData[0].isEmpty()) {
            view.showToastMessage(context.getString(R.string.please_add_plate_number));
            return;
        }
        this.requestPhotosPaths(context, editTextsData, spinnersData, location, isSaving, isSynced, isRejected);
    }

    private void requestPhotosPaths(Context context, String[] editTextsData, int[] spinnersData, String location, boolean isSaving, boolean isSynced, boolean isRejected) {
        /*
            String plateNumber[0]*, String houseConnNumber[3]*, String amrMeterBarcode[1]*, String waterMeterNumber[4]*,
            String waterMeterReading[5]*, int remarks[0]*, int waterMeterStatus[1]*, int meterType[2]*, int meterDiameter[3]*, int reducerDiameter[4]*,
            int openClose[5]*, int mechElec[6]*, int valveType[7]*, int coverExists[8]*, int position[9]*, int meterTypeBox[10]*, int pipeAfterMeter[11]*,
            int insideOutside[12]*, int pipeMaterial[13]*, int pipeSize[14]*, int sewerConnection[15]*, String numberOfElectricMeters[6]*,
            String electricMeterNumberLowest[7]*, String postalCode[8]*, String numberOfFloors[9]*, int buildingCategories[16]*,
            int commercial[17]*, int government[19]*, int residential[18]*, int district[20]*, String waselNumber[10]*, String amanaBarcode[2]*,
            String parcelID[11]*, String oldAccountNumber[12]*, String partnerNumber[13]*, String numberOfWaterConnections[14]*,
            [15]String numberOfUnits*, Map<String, String> photos
         */
        if (isRejected) this.requestSaveData(context, new Data(editTextsData[0],
                editTextsData[3], editTextsData[1], editTextsData[4],
                editTextsData[5], spinnersData[0], spinnersData[1], spinnersData[2], spinnersData[3], spinnersData[4], spinnersData[5], spinnersData[6],
                spinnersData[7], spinnersData[8], spinnersData[9], spinnersData[10], spinnersData[11], spinnersData[12], spinnersData[13], spinnersData[14], spinnersData[15],
                editTextsData[6], editTextsData[7], editTextsData[8],
                editTextsData[9], spinnersData[16], spinnersData[17], spinnersData[19], spinnersData[18], spinnersData[20],
                editTextsData[10], editTextsData[2], editTextsData[11], editTextsData[12],
                editTextsData[13], editTextsData[14], editTextsData[15], editTextsData[16], location,
                paths));
        else
            this.requestSaveData(context, new Data(editTextsData[0],
                    editTextsData[3], editTextsData[1], editTextsData[4],
                    editTextsData[5], spinnersData[0], spinnersData[1], spinnersData[2], spinnersData[3], spinnersData[4], spinnersData[5], spinnersData[6],
                    spinnersData[7], spinnersData[8], spinnersData[9], spinnersData[10], spinnersData[11], spinnersData[12], spinnersData[13], spinnersData[14], spinnersData[15],
                    editTextsData[6], editTextsData[7], editTextsData[8],
                    editTextsData[9], spinnersData[16], spinnersData[17], spinnersData[19], spinnersData[18], spinnersData[20],
                    editTextsData[10], editTextsData[2], editTextsData[11], editTextsData[12],
                    editTextsData[13], editTextsData[14], editTextsData[15], editTextsData[16], location,
                    paths), isSaving, isSynced);
    }


    //[0-9]+(\.[0-9]+
    @Override
    public void requestSaveData(Context context, Data data) {

        if (!data.getPlateNumber().isEmpty() && !data.getPlateNumber().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_plate_number_validation));
            return;
        }
        if (!data.getWaterMeterReading().isEmpty() && (!data.getWaterMeterReading().matches("[0-9]+") && !data.getWaterMeterReading().matches("[0-9]+(\\.[0-9]+)"))) {
            view.showToastMessage(context.getString(R.string.check_water_meter_reading_validation));
            return;
        }
        if (!data.getNumberOfElectricMeters().isEmpty() && !data.getNumberOfElectricMeters().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_number_of_electric_meters_validation));
            return;
        }
        if (!data.getPostalCode().isEmpty() && !data.getPostalCode().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_postal_code_validation));
            return;
        }
        if (!data.getNumberOfFloors().isEmpty() && !data.getNumberOfFloors().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_number_of_floors_validation));
            return;
        }
        if (!data.getParcelID().isEmpty() && !data.getParcelID().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_parcel_id_validation));
            return;
        }
        if (!data.getOldAccountNumber().isEmpty() && !data.getOldAccountNumber().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_old_account_number_validation));
            return;
        }
        if (!data.getPartnerNumber().isEmpty() && !data.getPartnerNumber().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_partner_number_validation));
            return;
        }
        if (!data.getNumberOfWaterConnections().isEmpty() && !data.getNumberOfWaterConnections().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_number_of_water_connections_validation));
            return;
        }
        if (!data.getNumberOfUnits().isEmpty() && !data.getNumberOfUnits().matches("[0-9]+")) {
            view.showToastMessage(context.getString(R.string.check_number_of_units_validation));
            return;
        }


        view.showProgress();
        if (data.getLocation().equals(",")) data.setLocation("");
        data.setDistrict(view.getDistrictId());
        data.setDistrictName(view.getDistrictName());
        data.setId(view.getId());
        model.syncRecord(context, this, data, true);

    }

    @Override
    public void requestSaveData(Context context, Data data, boolean isSaving, boolean isSynced) {
        if (data.getLocation().equals(",")) data.setLocation("");
        if (!isSaving) data.setId(view.getId());
        data.setDistrict(view.getDistrictId());
        data.setDistrictName(view.getDistrictName());
        /*
                        <item>N/A</item>
                        <item>Royal</item>
                        <item>Commercial</item>
                        <item>Government</item>
                        <item>Residential</item>
                        <item>tanker</item>
                         */

        if (data.getBuildingCategories() == 0 || data.getBuildingCategories() == 1 || data.getBuildingCategories() == 5) {
            data.setCommercial(-1);
            data.setResidential(-1);
            data.setGovernment(-1);
        } else if (data.getBuildingCategories() == 2) {
            data.setResidential(-1);
            data.setGovernment(-1);
        } else if (data.getBuildingCategories() == 3) {
            data.setCommercial(-1);
            data.setResidential(-1);
        } else {
            data.setCommercial(-1);
            data.setGovernment(-1);
        }
        if (isSynced) {
            if (!data.getPlateNumber().isEmpty() && !data.getPlateNumber().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_plate_number_validation));
                return;
            }
            if (!data.getWaterMeterReading().isEmpty() && (!data.getWaterMeterReading().matches("[0-9]+") && !data.getWaterMeterReading().matches("[0-9]+(\\.[0-9]+)"))) {
                view.showToastMessage(context.getString(R.string.check_water_meter_reading_validation));
                return;
            }
            if (!data.getNumberOfElectricMeters().isEmpty() && !data.getNumberOfElectricMeters().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_number_of_electric_meters_validation));
                return;
            }
            if (!data.getPostalCode().isEmpty() && !data.getPostalCode().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_postal_code_validation));
                return;
            }
            if (!data.getNumberOfFloors().isEmpty() && !data.getNumberOfFloors().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_number_of_floors_validation));
                return;
            }

            if (!data.getParcelID().isEmpty() && !data.getParcelID().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_parcel_id_validation));
                return;
            }
            if (!data.getOldAccountNumber().isEmpty() && !data.getOldAccountNumber().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_old_account_number_validation));
                return;
            }
            if (!data.getPartnerNumber().isEmpty() && !data.getPartnerNumber().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_partner_number_validation));
                return;
            }
            if (!data.getNumberOfWaterConnections().isEmpty() && !data.getNumberOfWaterConnections().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_number_of_water_connections_validation));
                return;
            }
            if (!data.getNumberOfUnits().isEmpty() && !data.getNumberOfUnits().matches("[0-9]+")) {
                view.showToastMessage(context.getString(R.string.check_number_of_units_validation));
                return;
            }


            view.showProgress();
            model.syncRecord(context, this, data, false);
        } else if (isSaving) {
            if (!data.getWaterMeterReading().isEmpty() && (!data.getWaterMeterReading().matches("[0-9]+(\\.[0-9]+)") && !data.getWaterMeterReading().matches("[0-9]+"))) {
                view.showToastMessage(context.getString(R.string.check_your_data_validation));
                return;
            }
            model.save(context, this, data);
        } else {
            if (!data.getWaterMeterReading().isEmpty() && (!data.getWaterMeterReading().matches("[0-9]+(\\.[0-9]+)") && !data.getWaterMeterReading().matches("[0-9]+"))) {
                view.showToastMessage(context.getString(R.string.check_your_data_validation));
                return;
            }
            model.editData(context, this, data);
        }
    }


    private final static int MY_PERMISSION_FINE_LOCATION = 2;
    private final static int PLACE_PICKER_REQUEST = 99;

    @Override
    public void requestAddNewLocation(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
            return;
        }
        try {
            PlacePicker.IntentBuilder placePicker = new PlacePicker.IntentBuilder();
            Intent intent = placePicker.build(activity);
            activity.startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e("LocationsActivity", String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("LocationsActivity", String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e("LocationsActivity", String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }

    @Override
    public void requestCheckPlateNumber(Context context, String plateNumber) {
        model.checkPlateNumber(context, this, plateNumber);
    }

    private Uri createImageFile(byte[] imageBytes) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File imageFolder = new File(Environment.getExternalStorageDirectory(), "GisGathering");
        imageFolder.mkdir();
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                imageFolder      // directory
        );

        FileOutputStream fileOutputStream = new FileOutputStream(image);
        fileOutputStream.write(imageBytes);
        fileOutputStream.close();
        return Uri.fromFile(image);
//        return Uri.parse(image.getAbsolutePath());
    }


    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public String getPath(Context context, Uri uri) {
        if (uri == null) return "";
        String result = "";
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    public void onResponse(Context context, boolean flag) {
        if (flag) {
            bitmap = null;
            filePath = null;
            paths.clear();
            view.onSaveReadingSuccess(context.getString(R.string.data_saved_successfully));
        } else
            view.showToastMessage(context.getString(R.string.failed_to_add_new_data_try_again));
    }

    @Override
    public void onResponse(Context context, Data data, boolean flag) {
        if (flag)
            view.onEditReadingSuccess(context.getString(R.string.data_edited_successfully), data);
        else
            view.showToastMessage(context.getString(R.string.failed_to_edit_data_try_again));
    }

    // handling response of SyncData ...
    @Override
    public void onSuccess(Context context, String responseString, Data data, boolean isRejected) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        JSONArray array = response.getJSONArray("response");
        if (array.getJSONObject(0).getString("status").equals("success")) {
            // data success upload its photos ...
            if ((data.getPhotos().get("meterConnectionFullView") != null && data.getPhotos().get("meterConnectionFullView").contains("//////"))
                    || (data.getPhotos().get("meterCloseUpView") != null && data.getPhotos().get("meterCloseUpView").contains("//////"))
                    || (data.getPhotos().get("electricMeterPhoto") != null && data.getPhotos().get("electricMeterPhoto").contains("//////"))
                    || (data.getPhotos().get("propertyPhoto") != null && data.getPhotos().get("propertyPhoto").contains("//////"))
                    || (data.getPhotos().get("amanaPlatePhoto") != null && data.getPhotos().get("amanaPlatePhoto").contains("//////"))
                    || (data.getPhotos().get("NWCPlatePhoto") != null && data.getPhotos().get("NWCPlatePhoto").contains("//////")))
                model.syncPics(context, this, data.getPhotos(), data.getPlateNumber(), isRejected);
            else {
                view.hideProgress();
                if (isRejected)
                    view.onRejectedUpdated();
                view.showToastMessage(context.getString(R.string.done_successfully));
            }
        } else {
            view.hideProgress();
            view.showToastMessage(array.getJSONObject(0).getString("message"));
        }
    }


    @Override
    public void onFailed(final Context context, VolleyError error, int index) {
        view.hideProgress();
        if (index == 1) {
            view.loadDistricts(DataDbHelper.getmInstance(context).getDistricts());
            return;
        }

        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        if (error instanceof AuthFailureError) {
            view.showToastMessage(context.getString(R.string.your_session_ended_please_relogin));
            view.goLogin();
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }


    // handling response of getting districts request ...
    @Override
    public void onSuccess1(Context context, String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        ArrayList<District> districts = new ArrayList<>();
        JSONArray data = response.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            districts.add(new District(data.getJSONObject(i).getInt("id"), data.getJSONObject(i).getString("name")));
        }

        // load districts spinner ....
        if (districts.isEmpty())
            districts.add(new District(-1, context.getString(R.string.n_a)));
        view.loadDistricts(districts);
    }


    @Override
    public void onUploadSuccess(Context context, JSONArray response, boolean isRejected, ArrayList<String> paths) throws JSONException {
        view.hideProgress();
        for (int i = 0; i < response.length(); i++) {
            if (!response.getJSONObject(i).getString("status").equals("success")) {
                view.showToastMessage(context.getString(R.string.failed_to_sync_this_record));
                return;
            }
        }

        for (int i = 0; i < paths.size(); i++) {
            deleteImage(context, paths.get(i));
        }
        view.showToastMessage(context.getString(R.string.done_successfully));
        if (isRejected)
            view.onRejectedUpdated();
    }


    public void deleteImage(Context context, String path) {
        Log.e("path", path);
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted 1:" + path);
                callBroadCast(context);
            } else {
                Log.e("-->", "file not Deleted 1:" + path);
            }
        }
    }

    public void callBroadCast(Context context) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            view.sendBroadCast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    @Override
    public void onUploadFailed(String message) {
        view.hideProgress();
        view.showToastMessage(message);
    }

    @Override
    public void onCheckSuccess(Context context, String response, String plateNumber) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        String status = jsonObject.getString("status");
        if (status.toLowerCase().equals("success"))
            view.goGetInfo(plateNumber);

    }


}
