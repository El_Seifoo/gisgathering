package com.gisgathering.neomit.seif.gisgathering.offline.details;

import android.content.Context;
import android.util.Log;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public class MoreDetailsPresenter implements MoreDetailsMVP.Presenter {
    private MoreDetailsMVP.View view;

    public MoreDetailsPresenter(MoreDetailsMVP.View view) {
        this.view = view;
    }


    @Override
    public void requestSpinnersStrings(Context context, Data data) {
        ArrayList<MoreDetails> list = new ArrayList<>();
        list.add(new MoreDetails(context.getString(R.string.plate_number), data.getPlateNumber()));
        list.add(new MoreDetails(context.getString(R.string.house_connection_number), data.getHouseConnNumber()));
        list.add(new MoreDetails(context.getString(R.string.amr_meter_barcode), data.getAmrMeterBarcode()));
        list.add(new MoreDetails(context.getString(R.string.water_meter_number), data.getWaterMeterNumber()));
        list.add(new MoreDetails(context.getString(R.string.water_meter_reading), data.getWaterMeterReading()));
        list.add(new MoreDetails(context.getString(R.string.remarks), returnCorrespondingStringsOne(data.getRemarks(), context.getResources().getStringArray(R.array.remarks))));
        list.add(new MoreDetails(context.getString(R.string.water_meter_status), returnCorrespondingStringsTwo(data.getWaterMeterStatus(), context.getResources().getStringArray(R.array.water_meter_status))));
        list.add(new MoreDetails(context.getString(R.string.meter_type), returnCorrespondingStringsOne(data.getMeterType(), context.getResources().getStringArray(R.array.meter_type))));
        list.add(new MoreDetails(context.getString(R.string.meter_diameter), returnCorrespondingStringsOne(data.getMeterDiameter(), context.getResources().getStringArray(R.array.meter_diameter_pipe_size))));
        list.add(new MoreDetails(context.getString(R.string.reducer_diameter), returnCorrespondingStringsOne(data.getReducerDiameter(), context.getResources().getStringArray(R.array.reduce_diameter))));
        list.add(new MoreDetails(context.getString(R.string.open_close), returnCorrespondingStringsOne(data.getOpenClose(), context.getResources().getStringArray(R.array.open_close))));
        list.add(new MoreDetails(context.getString(R.string.mech_elec), returnCorrespondingStringsOne(data.getMechElec(), context.getResources().getStringArray(R.array.mech_elec))));
        list.add(new MoreDetails(context.getString(R.string.valve_type), returnCorrespondingStringsOne(data.getValveType(), context.getResources().getStringArray(R.array.valve_type))));
        list.add(new MoreDetails(context.getString(R.string.cover_exists), returnCorrespondingStringsOne(data.getCoverExists(), context.getResources().getStringArray(R.array.cover_exist))));
        list.add(new MoreDetails(context.getString(R.string.position), returnCorrespondingStringsOne(data.getPosition(), context.getResources().getStringArray(R.array.position))));
        list.add(new MoreDetails(context.getString(R.string.meter_type_box), returnCorrespondingStringsOne(data.getMeterTypeBox(), context.getResources().getStringArray(R.array.meter_type_box))));
        list.add(new MoreDetails(context.getString(R.string.pipe_after_meter), returnCorrespondingStringsOne(data.getPipeAfterMeter(), context.getResources().getStringArray(R.array.pipe_after_meter))));
        list.add(new MoreDetails(context.getString(R.string.inside_outside), returnCorrespondingStringsOne(data.getInsideOutside(), context.getResources().getStringArray(R.array.inside_outside))));
        list.add(new MoreDetails(context.getString(R.string.pipe_material), returnCorrespondingStringsOne(data.getPipeMaterial(), context.getResources().getStringArray(R.array.pipe_material))));
        list.add(new MoreDetails(context.getString(R.string.pipe_size), returnCorrespondingStringsOne(data.getPipeSize(), context.getResources().getStringArray(R.array.meter_diameter_pipe_size))));
        list.add(new MoreDetails(context.getString(R.string.sewer_connection), returnCorrespondingStringsOne(data.getSewerConnection(), context.getResources().getStringArray(R.array.sewer_connection))));
        list.add(new MoreDetails(context.getString(R.string.number_of_electric_meters), data.getNumberOfElectricMeters()));
        list.add(new MoreDetails(context.getString(R.string.electric_meter_number_lowest), data.getElectricMeterNumberLowest()));
        list.add(new MoreDetails(context.getString(R.string.postal_code), data.getPostalCode()));
        list.add(new MoreDetails(context.getString(R.string.number_of_floors), data.getNumberOfFloors()));
        list.add(new MoreDetails(context.getString(R.string.building_category), returnCorrespondingStringsOne(data.getBuildingCategories(), context.getResources().getStringArray(R.array.building_category))));
        /*
    <item>N/A</item>
        <item>Royal</item>
        <item>Commercial</item>
        <item>Government</item>
        <item>Residential</item>
        <item>tanker</item>
     */
        if (data.getBuildingCategories() == 2) {
            list.add(new MoreDetails(context.getString(R.string.sub_building_commercial), returnCorrespondingStringsFour(data.getCommercial(), context.getResources().getStringArray(R.array.commercial))));
        } else if (data.getBuildingCategories() == 4) {
            Log.e("data 4", "ID: " + data.getId() + ") \nbuilding: " + data.getBuildingCategories() + ",com: " + data.getCommercial() + " , gov: " +
                    data.getGovernment() + ", res: " + data.getResidential());
            list.add(new MoreDetails(context.getString(R.string.sub_building_residential), returnCorrespondingStringsOne(data.getResidential(), context.getResources().getStringArray(R.array.residential))));
        } else if (data.getBuildingCategories() == 3) {
            Log.e("data 3", "ID: " + data.getId() + ") \nbuilding: " + data.getBuildingCategories() + ",com: " + data.getCommercial() + " , gov: " +
                    data.getGovernment() + ", res: " + data.getResidential());
            list.add(new MoreDetails(context.getString(R.string.sub_building_government), returnCorrespondingStringsOne(data.getGovernment(), context.getResources().getStringArray(R.array.governmental))));
        }

        list.add(new MoreDetails(context.getString(R.string.district), data.getDistrictName()));
        list.add(new MoreDetails(context.getString(R.string.wasel_number), data.getWaselNumber()));
        list.add(new MoreDetails(context.getString(R.string.amana_barcode), data.getAmanaBarcode()));
        list.add(new MoreDetails(context.getString(R.string.parcel_id), data.getParcelID()));
        list.add(new MoreDetails(context.getString(R.string.old_account_number), data.getOldAccountNumber()));
        list.add(new MoreDetails(context.getString(R.string.partner_number), data.getPartnerNumber()));
        list.add(new MoreDetails(context.getString(R.string.number_of_water_connections), data.getNumberOfWaterConnections()));
        list.add(new MoreDetails(context.getString(R.string.number_of_units), data.getNumberOfUnits()));
        list.add(new MoreDetails(context.getString(R.string.zone_code), data.getZoneCode()));
        list.add(new MoreDetails(context.getString(R.string.location), data.getLocation()));
        int count = 0;
        if (data.getPhotos().get("meterConnectionFullView") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.meter_connection_full_view), data.getPhotos().get("meterConnectionFullView")));
        }
        if (data.getPhotos().get("meterCloseUpView") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.meter_close_up_view), data.getPhotos().get("meterCloseUpView")));
        }
        if (data.getPhotos().get("electricMeterPhoto") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.electric_meter_photo), data.getPhotos().get("electricMeterPhoto")));
        }
        if (data.getPhotos().get("propertyPhoto") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.property_photo), data.getPhotos().get("propertyPhoto")));
        }
        if (data.getPhotos().get("amanaPlatePhoto") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.amana_plate_photo), data.getPhotos().get("amanaPlatePhoto")));
        }
        if (data.getPhotos().get("NWCPlatePhoto") != null) {
            count++;
            list.add(new MoreDetails(context.getString(R.string.nwc_plate_photo), data.getPhotos().get("NWCPlatePhoto")));
        }

        if (count == 0)
            view.loadData(list, false, count);
        else view.loadData(list, true, count);
    }

    // for lists that ids -1,1,2,.....
    private String returnCorrespondingStringsOne(int id, String[] data) {
        if (id == -1) return data[0];
        else return data[id];
    }

    // for water meter status list ...
    private String returnCorrespondingStringsTwo(int id, String[] data) {
        if (id == -1) return data[0];
        else return data[id + 1];
    }

    // for lists that ids 1,2,...
    private String dddd(int id, String[] data) {
        return data[id - 1];
    }

    // for lists that ids -1,2,3,4,.....
    private String returnCorrespondingStringsFour(int id, String[] data) {
        if (id == -1) return data[0];
        else return data[id - 1];
    }


}
