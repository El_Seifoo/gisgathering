package com.gisgathering.neomit.seif.gisgathering.rejected;

import android.app.Activity;
import android.content.Context;

import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

public interface RejectedMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void loadRejectedList(ArrayList<Data> dataList);

        void showToastMessage(String message);

        void goLogin();

        void loadSearchedData(ArrayList<Data> searchedList);
    }

    interface Presenter {
        void requestReadingsList(Context context);

        void search(ArrayList<Data> dataList, String query);
    }
}
