package com.gisgathering.neomit.seif.gisgathering.rejected;

import android.content.Intent;
import android.content.res.Configuration;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataCollectionActivity;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.login.LoginActivity;
import com.gisgathering.neomit.seif.gisgathering.offline.details.MoreDetailsFragment;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.Collection;

public class RejectedActivity extends AppCompatActivity implements RejectedMVP.View, RejectedAdapter.ListItemClickListener {

    private EditText plateNumber;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private RejectedAdapter adapter;
    private ProgressBar progressBar;
    private FrameLayout fragmentContainer;
    private RejectedMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected);

        getSupportActionBar().setTitle(getString(R.string.rejected_data));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedPresenter(this, new RejectedModel());

        fragmentContainer = (FrameLayout) findViewById(R.id.frame);

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }
        adapter = new RejectedAdapter(this);


        plateNumber = (EditText) findViewById(R.id.plate_number);
        plateNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.search(dataList, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        presenter.requestReadingsList(getApplicationContext());
    }

    private final static int SCAN_QR_REQUEST = 2;

    public void scanQR(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, SCAN_QR_REQUEST);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListText() {
        adapter.clear();
        emptyListTextView.setText(getString(R.string.no_data_available));

    }

    ArrayList<Data> dataList;

    @Override
    public void loadRejectedList(ArrayList<Data> dataList) {
        this.dataList = dataList;
        emptyListTextView.setText("");
        adapter.setDataList(dataList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goLogin() {
        String username = MySingleton.getmInstance(this).getUserName();
        String ip = MySingleton.getmInstance(this).getUserIP();
        MySingleton.getmInstance(this).logout();
        MySingleton.getmInstance(this).saveUserIp(ip);
        MySingleton.getmInstance(this).saveUserName(username);
        DataDbHelper.getmInstance(this).deleteDistricts();
        Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent1);
    }

    @Override
    public void loadSearchedData(ArrayList<Data> searchedList) {
        emptyListTextView.setText("");
        adapter.setDataList(searchedList);
    }


    private static final int UPDATE_REJECTED_DATA_REQUEST = 1;

    @Override
    public void onListItemClick(int position, Data data, View view) {
        Intent edit = new Intent(getApplicationContext(), DataCollectionActivity.class);
        edit.putExtra("Editing", data);
        edit.putExtra("RejectedData", position);
        startActivityForResult(edit, UPDATE_REJECTED_DATA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("code", requestCode + "," + requestCode);
        if (requestCode == UPDATE_REJECTED_DATA_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeReading(this, data.getExtras().getInt("position"));
            Log.e("position", data.getExtras().getInt("position") + "");
            return;
        }
        if (requestCode == SCAN_QR_REQUEST) {
            IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
            if (result != null) {
                if (result.getContents() != null)
                    plateNumber.setText(result.getContents().replaceAll("[^0-9]", ""));
            }
            return;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment child = (MoreDetailsFragment) getSupportFragmentManager().findFragmentByTag("MoreDetails");
        if (child != null) {
            getSupportFragmentManager().beginTransaction().
                    remove(getSupportFragmentManager().findFragmentByTag("MoreDetails")).commit();
            fragmentContainer.setClickable(false);
            getSupportActionBar().setTitle(getString(R.string.offline));
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Fragment child = (MoreDetailsFragment) getSupportFragmentManager().findFragmentByTag("MoreDetails");
            if (child != null) {
                getSupportFragmentManager().beginTransaction().
                        remove(getSupportFragmentManager().findFragmentByTag("MoreDetails")).commit();
                fragmentContainer.setClickable(false);
                getSupportActionBar().setTitle(getString(R.string.offline));
                return true;
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
