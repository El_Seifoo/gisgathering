package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.infoMap.InfoMapActivity;
import com.gisgathering.neomit.seif.gisgathering.login.LoginActivity;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;

import java.util.Locale;


public class DataCollectionActivity extends AppCompatActivity implements DataCollectionMVP.View {
    private EditText plateNumber, amrMeterBarcode, amanaBarcode, houseConnectionNumber, waterMeterNumber, waterMeterReading,
            numberOfElectricMeters, electricMeterNumberLowest, postalCode, numberOfFloors, waselNumber, parcelId,
            oldAccountNumber, partnerNumber, numberOfWaterConnections, numberOfUnits, zoneCode, location;

    private TextView plateNumberLabel, amrMeterBarcodeLabel, amanaBarcodeLabel, houseConnectionNumberLabel, waterMeterNumberLabel, waterMeterReadingLabel,
            numberOfElectricMetersLabel, electricMeterNumberLowestLabel, postalCodeLabel, numberOfFloorsLabel, waselNumberLabel, parcelIdLabel,
            oldAccountNumberLabel, partnerNumberLabel, numberOfWaterConnectionsLabel, numberOfUnitsLabel, zoneCodeLabel, locationLabel, reason;

    private Spinner remarksSpinner, waterMeterStatusSpinner, meterTypeSpinner, meterDiameterSpinner, reducerDiameterSpinner,
            openCloseSpinner, mechElecSpinner, valveTypeSpinner, coverExistsSpinner, positionSpinner, meterTypeBoxSpinner,
            pipeAfterMeterSpinner, insideOutsideSpinner, pipeMaterialSpinner, pipeSizeSpinner, sewerConnectionSpinner,
            buildingCategorySpinner, subBuildingCommercialSpinner, subBuildingGovernmentSpinner, subBuildingResidentialSpinner,
            districtSpinner;
    private ImageView meterConnectionFullViewImage, meterCloseUpViewImage, electricMeterPhotoImage, propertyPhotoImage,
            amanaPlatePhotoImage, NWCPlatePhotoImage;
    private Button submit, locationBtn;


    private ProgressBar progressBar;

    private LinearLayout commercialContainer, residentialContainer, governmentalContainer;

    Spinner[] spinners;
    EditText[] editTexts;
    TextView[] textViews;
    ImageView[] imageViews;
    String[][] allLists;

    private DataCollectionMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String localLanguage = MySingleton.getmInstance(getApplicationContext()).getAppLang();
        if (localLanguage.equals(getString(R.string.settings_language_arabic_value))) {
            setLocale("ar", this);
        } else if (localLanguage.equals(getString(R.string.settings_language_english_value))) {
            setLocale("en", this);
        }

        setContentView(R.layout.activity_data_collection);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.collecting_data));

        presenter = new DataCollectionPresenter(this, new DataCollectionModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        // EditTexts declaration ...
        location = (EditText) findViewById(R.id.location);
        editTexts = new EditText[]{plateNumber, amrMeterBarcode, amanaBarcode, houseConnectionNumber, waterMeterNumber, waterMeterReading,
                numberOfElectricMeters, electricMeterNumberLowest, postalCode, numberOfFloors, waselNumber, parcelId, oldAccountNumber,
                partnerNumber, numberOfWaterConnections, numberOfUnits, zoneCode};
        int[] editTextsIds = {R.id.plate_number, R.id.amr_meter_barcode, R.id.amana_barcode, R.id.house_connection_number, R.id.water_meter_number,
                R.id.water_meter_reading, R.id.number_of_electric_meters, R.id.electric_meter_number_lowest, R.id.postal_code, R.id.number_of_floors,
                R.id.wasel_number, R.id.parcel_id, R.id.old_account_number, R.id.partner_number, R.id.number_of_water_connections, R.id.number_of_units, R.id.zone_code};

        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i] = (EditText) findViewById(editTextsIds[i]);
        }
        if (getIntent().hasExtra("Add"))
            handlePlateNumberEditTextEditor(editTexts[0]);

        // TextViews declaration ...
        textViews = new TextView[]{plateNumberLabel, amrMeterBarcodeLabel, amanaBarcodeLabel, houseConnectionNumberLabel, waterMeterNumberLabel, waterMeterReadingLabel,
                numberOfElectricMetersLabel, electricMeterNumberLowestLabel, postalCodeLabel, numberOfFloorsLabel, waselNumberLabel, parcelIdLabel,
                oldAccountNumberLabel, partnerNumberLabel, numberOfWaterConnectionsLabel, numberOfUnitsLabel, zoneCodeLabel};
        int[] textViewIds = {R.id.plate_number_label, R.id.amr_meter_barcode_label, R.id.amana_barcode_label, R.id.house_connection_number_label, R.id.water_meter_number_label,
                R.id.water_meter_reading_label, R.id.number_of_electric_meters_label, R.id.electric_meter_number_lowest_label, R.id.postal_code_label, R.id.number_of_floors_label,
                R.id.wasel_number_label, R.id.parcel_id_label, R.id.old_account_number_label, R.id.partner_number_label, R.id.number_of_water_connections_label, R.id.number_of_units_label, R.id.zone_code_label};

        for (int i = 0; i < textViews.length; i++) {
            textViews[i] = (TextView) findViewById(textViewIds[i]);
        }

        reason = (TextView) findViewById(R.id.reason);

        commercialContainer = (LinearLayout) findViewById(R.id.commercial_container);
        residentialContainer = (LinearLayout) findViewById(R.id.residential_container);
        governmentalContainer = (LinearLayout) findViewById(R.id.governmental_container);


        // spinners declaration ...
        spinners = new Spinner[]{remarksSpinner, waterMeterStatusSpinner, meterTypeSpinner, meterDiameterSpinner, reducerDiameterSpinner, openCloseSpinner,
                mechElecSpinner, valveTypeSpinner, coverExistsSpinner, positionSpinner, meterTypeBoxSpinner, pipeAfterMeterSpinner, insideOutsideSpinner,
                pipeMaterialSpinner, pipeSizeSpinner, sewerConnectionSpinner, buildingCategorySpinner, subBuildingCommercialSpinner, subBuildingGovernmentSpinner,
                subBuildingResidentialSpinner, districtSpinner};
        final int[] spinnersIds = {R.id.remarks_spinner, R.id.water_meter_status_spinner, R.id.meter_type_spinner, R.id.meter_diameter_spinner,
                R.id.reducer_diameter_spinner, R.id.open_close_spinner, R.id.mech_elec_spinner, R.id.valve_type_spinner,
                R.id.cover_exists_spinner, R.id.position_spinner, R.id.meter_type_box_spinner, R.id.pipe_after_meter_spinner,
                R.id.inside_outside_spinner, R.id.pipe_material_spinner, R.id.pipe_size_spinner, R.id.sewer_connection_spinner,
                R.id.building_category_spinner, R.id.sub_building_commercial_spinner, R.id.sub_building_government_spinner,
                R.id.sub_building_residential_spinner, R.id.district_spinner};

        for (int i = 0; i < spinners.length; i++) {
            spinners[i] = (Spinner) findViewById(spinnersIds[i]);
        }

        allLists = new String[][]{getResources().getStringArray(R.array.remarks), getResources().getStringArray(R.array.water_meter_status),
                getResources().getStringArray(R.array.meter_type), getResources().getStringArray(R.array.meter_diameter_pipe_size), getResources().getStringArray(R.array.reduce_diameter),
                getResources().getStringArray(R.array.open_close), getResources().getStringArray(R.array.mech_elec), getResources().getStringArray(R.array.valve_type),
                getResources().getStringArray(R.array.cover_exist), getResources().getStringArray(R.array.position), getResources().getStringArray(R.array.meter_type_box),
                getResources().getStringArray(R.array.pipe_after_meter), getResources().getStringArray(R.array.inside_outside), getResources().getStringArray(R.array.pipe_material),
                getResources().getStringArray(R.array.meter_diameter_pipe_size), getResources().getStringArray(R.array.sewer_connection), getResources().getStringArray(R.array.building_category),
                getResources().getStringArray(R.array.commercial), getResources().getStringArray(R.array.governmental), getResources().getStringArray(R.array.residential)};

        presenter.requestDistricts(getApplicationContext());

        // images declaration ...
        imageViews = new ImageView[]{meterConnectionFullViewImage, meterCloseUpViewImage, electricMeterPhotoImage,
                propertyPhotoImage, amanaPlatePhotoImage, NWCPlatePhotoImage};
        int[] imageViewsIds = {R.id.meter_connection_full_view_image, R.id.meter_close_up_view_image, R.id.electric_meter_photo_image,
                R.id.property_photo_image, R.id.amana_plate_photo_image, R.id.nwc_plate_photo_image};

        for (int i = 0; i < imageViews.length; i++) {
            imageViews[i] = (ImageView) findViewById(imageViewsIds[i]);
        }

        locationBtn = (Button) findViewById(R.id.location_btn);
        locationBtn = (Button) findViewById(R.id.location_btn);
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickLocation();
            }
        });
        submit = (Button) findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().hasExtra("RejectedData"))
                    presenter.preRequestSaveData(getApplicationContext(), editTexts, spinners, location.getText().toString().trim());
                else
                    presenter.preRequestSaveData(getApplicationContext(), editTexts, spinners, location.getText().toString().trim(), !getIntent().hasExtra("Editing"), getIntent().hasExtra("NotOffline"));
            }
        });

        //check if user is editing data or not ...
        presenter.requestEditingData(this, getIntent().hasExtra("Editing"));
    }

    private void handlePlateNumberEditTextEditor(EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!((EditText) v).getText().toString().isEmpty() && !hasFocus) {
                    presenter.requestCheckPlateNumber(getApplicationContext(), ((EditText) v).getText().toString());
                }
            }
        });
    }


    private void pickLocation() {
        presenter.requestAddNewLocation(this);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }


    public void scanQR(View view) {
        presenter.whichView(this, view, false);
    }

    public void pickImage(View view) {
        for (int i = 0; i < editTexts.length; i++) {
            if (editTexts[i].isFocused()) {
                editTexts[i].clearFocus();
                break;
            }
        }
        presenter.whichView(this, view, true);
    }


    @Override
    public void loadEditingDataEditTexts(Data data, String[] editTextsData, boolean[] flags) {
        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i].setText(editTextsData[i]);
            textViews[i].setVisibility(!flags[i] ? View.GONE : View.VISIBLE);
        }

        if (data.getReason() != null)
            if (getIntent().hasExtra("RejectedData") || !data.getReason().isEmpty()) {
                reason.setVisibility(View.VISIBLE);
                reason.setText(getString(R.string.reason) + ": " + data.getReason());
            }

        location.setText(data.getLocation());
        presenter.requestEditingPhotos(data.getPhotos(), imageViews, getIntent().hasExtra("NotOffline"), getIntent().hasExtra("RejectedData"));
    }

    @Override
    public void setEditingImages(ImageView imageView, Bitmap path) {
        Glide.with(getApplicationContext())
                .asBitmap()
                .load(path)
                .apply(new RequestOptions()
                        .error(R.mipmap.logo))
                .into(imageView);
    }

    @Override
    public void setEditingImages(final ImageView imageView, final String path) {
//        Glide.with(getApplicationContext())
//                .load(path)
//                .apply(new RequestOptions()
//                        .error(R.mipmap.logo))
//                .into(imageView);

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                setEditingImages(imageView, path.toLowerCase().replace(".png", ".jpg"));
            }
        };
        Glide.with(getApplicationContext())
                .load(path)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        if (isFirstResource && path.toLowerCase().contains(".png")) {
                            handler.post(runnable);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        handler.removeCallbacks(runnable);
                        return false;
                    }
                })
                .apply(new RequestOptions()
                        .error(R.mipmap.logo))
                .into(imageView);
    }

    int[] districtsIds;

    @Override
    public void loadDistricts(ArrayList<District> districts) {
        int selectedPosition = 0;
        districtsIds = new int[districts.size()];
        String[] districtString = new String[districts.size()];
        for (int i = 0; i < districts.size(); i++) {
            districtString[i] = (districts.get(i).getName());
            districtsIds[i] = districts.get(i).getId();
            if (getIntent().hasExtra("Editing") &&
                    ((Data) getIntent().getSerializableExtra("Editing")).getDistrict() == (districts.get(i).getId())) {
                selectedPosition = i;
            }
        }
        ArrayAdapter<String> districtsAdapter;
        districtsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtString);
        spinners[20].setAdapter(districtsAdapter);
        spinners[20].setSelection(selectedPosition);


        // set all lists to spinners ...
        presenter.requestAllSpinnersList(this, spinners, allLists, getIntent().hasExtra("Editing"));
    }

    @Override
    public void setSpinnersData(Spinner spinner, ArrayAdapter<String> adapter, int position) {
        spinner.setAdapter(adapter);
        spinner.setSelection(position);
    }

    @Override
    public void setSpinnersData(Spinner spinner, ArrayAdapter<String> adapter) {
        spinner.setAdapter(adapter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initializeScanner(IntentIntegrator intentIntegrator, int flag) {
        Intent intent = intentIntegrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setQRData(EditText editText, String data) {
        editText.setText(data);
    }

    @Override
    public void startPickingPhotoForResult(Intent intent, int request) {
        startActivityForResult(Intent.createChooser(intent, "Select Image"), request);
    }

    @Override
    public void loadSelectedPic(byte[] image, ImageView imageView) {

        Glide.with(getApplicationContext())
                .asBitmap()
                .load(image)
                .apply(new RequestOptions())
                .into(imageView);
    }

    @Override
    public void onSaveReadingSuccess(String message) {
        showToastMessage(message);
        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i].setText("");
        }

        for (int i = 0; i < spinners.length; i++) {
            spinners[i].setSelection(0);
        }

        for (int i = 0; i < imageViews.length; i++) {
            imageViews[i].setImageResource(R.mipmap.logo);
        }

    }


    @Override
    public void onEditReadingSuccess(String message, Data data) {
        showToastMessage(message);
        Intent returnToReadingsList = new Intent();
        returnToReadingsList.putExtra("UpdatedData", data);
        setResult(RESULT_OK, returnToReadingsList);
        finish();
    }

    @Override
    public void onRejectedUpdated() {
        Intent returnToRejectedList = new Intent();
        returnToRejectedList.putExtra("position", getIntent().getExtras().getInt("RejectedData"));
        setResult(RESULT_OK, returnToRejectedList);
        finish();
    }

    @Override
    public void setLocation(String loc) {
        location.setText(loc);
    }

    @Override
    public void showHideBuildingCategs(boolean comm, boolean res, boolean gov, boolean tanker) {
        commercialContainer.setVisibility(tanker ? View.GONE : comm ? View.VISIBLE : View.GONE);
        residentialContainer.setVisibility(tanker ? View.GONE : res ? View.VISIBLE : View.GONE);
        governmentalContainer.setVisibility(tanker ? View.GONE : gov ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getId() {
        return ((Data) getIntent().getSerializableExtra("Editing")).getId();
    }

    @Override
    public int getDistrictId() {
        return districtsIds[spinners[20].getSelectedItemPosition()];
    }

    @Override
    public String getDistrictName() {
        return spinners[20].getSelectedItem().toString();
    }

    @Override
    public void goLogin() {
        String username = MySingleton.getmInstance(this).getUserName();
        String ip = MySingleton.getmInstance(this).getUserIP();
        MySingleton.getmInstance(this).logout();
        MySingleton.getmInstance(this).saveUserIp(ip);
        MySingleton.getmInstance(this).saveUserName(username);
        DataDbHelper.getmInstance(this).deleteDistricts();
        Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent1);
    }

    @Override
    public void goGetInfo(String plateNumber) {
        showOptionDialog(plateNumber);
    }

    private void showOptionDialog(final String plateNumber) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage(getString(R.string.plate_number_already_exist_check_its_data));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), InfoMapActivity.class);
                intent.putExtra("plateNumber", plateNumber);
                intent.putExtra("GetInfo", "GetInfo");
                startActivity(intent);
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                editTexts[0].setText("");
                editTexts[0].requestFocus();
            }
        });


        AlertDialog mDialog = builder.create();
        mDialog.show();
    }

    @Override
    public void clearFocus() {
        for (int i = 0; i < editTexts.length; i++) {
            if (editTexts[i].isFocused()) {
                editTexts[i].clearFocus();
                break;
            }
        }
    }

    @Override
    public void sendBroadCast(Intent intent) {
        sendBroadcast(intent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        presenter.onRequestPermissionsResult(this, requestCode, permissions, grantResults, imageViews);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(getApplicationContext(), requestCode, resultCode, data, imageViews, new EditText[]{editTexts[0], editTexts[3], editTexts[1], editTexts[2]});
    }


    /*
     *  Change the configuration of the Application depending on App Language
     */
    private void setLocale(String lang, Context context) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            getApplicationContext().getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
