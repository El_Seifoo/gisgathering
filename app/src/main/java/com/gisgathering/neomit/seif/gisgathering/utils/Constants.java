package com.gisgathering.neomit.seif.gisgathering.utils;

/**
 * Created by sief on 7/26/2018.
 */

public class Constants {
    public static final String SHARED_PREF_NAME = "GisGathering";
    public static final String USER_TOKEN = "token";
    public static final String USER_NAME = "username";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String APP_LANGUAGE = "appLang";
    public static final String USER_IP = "ip";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
}
