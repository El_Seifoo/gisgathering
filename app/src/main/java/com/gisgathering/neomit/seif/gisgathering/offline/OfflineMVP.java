package com.gisgathering.neomit.seif.gisgathering.offline;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public interface OfflineMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void loadDataList(ArrayList<Data> dataList);

        void showToastMessage(String message);

        void updateList(int id);

        void showMoreDetails(Data data);

        void editData(Data data);

        void updateDataItemView(Data data);
        void goLogin();

        void sendBroadCast(Intent intent);
    }

    interface Presenter {
//        void preRequestReadingList(Activity activity, boolean isRetrieving);

        void requestReadingsList(Activity activity, boolean isSyncing);

        void requestRemoveReadingById(Context context, int id);

        void onActivityResult(int requestCode, int editDataRequestCode, int resultCode, Intent data);

        void onListItemClicked(Context context, Data data, android.view.View view);
    }
}
