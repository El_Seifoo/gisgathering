package com.gisgathering.neomit.seif.gisgathering.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.Holder> {
    private ArrayList<Data> dataList;
    final private ListItemClickListener mOnClickListener;

    public DataAdapter(ListItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public interface ListItemClickListener {
        void onListItemClick(Data reading, View view);
    }

    public void setData(ArrayList<Data> dataList) {
        this.dataList = dataList;
    }

    public void removeReading(Context context, int id) {
        if (dataList != null && dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).getId() == id) {
                    dataList.remove(i);
                    notifyItemRemoved(i);
                    break;
                }
            }
        }
        if (dataList != null && dataList.isEmpty()) {
            ((OfflineActivity) context).showEmptyListText();
        }
    }

    public void updateReading(Data updatedData) {
        for (int i = 0; i < dataList.size(); i++) {
            if (updatedData.getId() == dataList.get(i).getId()) {
                dataList.set(i, updatedData);
                notifyDataSetChanged();
                break;
            }
        }
    }

    @NonNull
    @Override
    public DataAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.data_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.Holder holder, int position) {
        /*
            plateNumber
            houseConnNumber
            amrMeterBarcode
         */
        String numbers = "";
        if (!dataList.get(position).getPlateNumber().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </font>" + dataList.get(position).getPlateNumber() + "<br>";
        if (!dataList.get(position).getHouseConnNumber().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.house_connection_number) + ": </font>" + dataList.get(position).getHouseConnNumber() + "<br>";
        if (!dataList.get(position).getAmrMeterBarcode().isEmpty())
            numbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.amr_meter_barcode) + ": </font>" + dataList.get(position).getAmrMeterBarcode() + "<br>";
        holder.numbers.setText(Html.fromHtml(numbers.substring(0, numbers.length() - 4)), TextView.BufferType.SPANNABLE);
    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView numbers, moreDetails;
        ImageView edit, remove;
        Button sync;

        public Holder(View itemView) {
            super(itemView);
            numbers = (TextView) itemView.findViewById(R.id.numbers);
            moreDetails = (TextView) itemView.findViewById(R.id.more_info);
            edit = (ImageView) itemView.findViewById(R.id.edit_data);
            remove = (ImageView) itemView.findViewById(R.id.remove_data);
            sync = (Button) itemView.findViewById(R.id.sync_button);

            moreDetails.setOnClickListener(this);
            edit.setOnClickListener(this);
            remove.setOnClickListener(this);
            sync.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() >= 0)
                mOnClickListener.onListItemClick(dataList.get(getAdapterPosition()), v);
        }
    }
}
