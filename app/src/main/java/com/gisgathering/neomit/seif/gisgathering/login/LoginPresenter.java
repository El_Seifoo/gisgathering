package com.gisgathering.neomit.seif.gisgathering.login;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.District;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by sief on 7/16/2018.
 */

public class LoginPresenter implements LoginMVP.Presenter, LoginModel.VolleyCallback {
    private LoginMVP.View view;
    private LoginModel model;

    public LoginPresenter(LoginMVP.View view, LoginModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void checkIfUserLoggedIn(boolean isLoggedIn) {
        if (!isLoggedIn) return;
        view.goHome();
    }

    @Override
    public void requestLogin(Context context, String username, String password, String ip) {
        if (username.isEmpty() || password.isEmpty() || ip.isEmpty()) {
            view.showToastMessage(context.getString(R.string.please_fill_all_fields));
            return;
        }
        view.showProgress();
        model.login(context, this, username, password, ip);
    }

    @Override
    public void onSuccess(Context context, String response, String userId, String ip) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.has("access_token")) {
            String token = jsonObject.getString("access_token");
            MySingleton.getmInstance(context).saveUserToken(token);
            MySingleton.getmInstance(context).saveUserName(userId);
            MySingleton.getmInstance(context).saveUserIp(ip);
            MySingleton.getmInstance(context).loginUser();
            model.getDistricts(context, this, token);
        } else view.showToastMessage(context.getString(R.string.invalid_username_or_password));
    }

    @Override
    public void onSuccess(Context context, String responseString, String token) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        ArrayList<District> districts = new ArrayList<>();
        JSONArray data = response.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            districts.add(new District(data.getJSONObject(i).getInt("id"), data.getJSONObject(i).getString("name")));
        }

        // load districts spinner ....
        if (!districts.isEmpty())
            DataDbHelper.getmInstance(context).addDistricts(districts);
        view.goHome();
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error  instanceof AuthFailureError){
            view.showToastMessage(context.getString(R.string.invalid_username_or_password));
            return;
        }
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection_or_your_host_address_is_wrong));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
