package com.gisgathering.neomit.seif.gisgathering.offline;

import android.content.Intent;
import android.content.res.Configuration;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataCollectionActivity;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.login.LoginActivity;
import com.gisgathering.neomit.seif.gisgathering.offline.details.MoreDetailsFragment;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class OfflineActivity extends AppCompatActivity implements OfflineMVP.View, DataAdapter.ListItemClickListener {

    private TextView emptyListTextView;
    private Button syncButton;
    private RecyclerView recyclerView;
    private DataAdapter adapter;
    private ProgressBar progressBar;
    private FrameLayout fragmentContainer;
    private OfflineMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);

        getSupportActionBar().setTitle(getString(R.string.offline));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new OfflinePresenter(this, new OfflineModel());

        fragmentContainer = (FrameLayout) findViewById(R.id.frame);

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        syncButton = (Button) findViewById(R.id.sync_button);

        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestReadingsList(OfflineActivity.this, true);
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.readings_recycler_view);
        recyclerView.setHasFixedSize(true);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }
        adapter = new DataAdapter(this);

        presenter.requestReadingsList(this, false);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListText() {
        emptyListTextView.setText(getString(R.string.no_data_available));
    }

    @Override
    public void loadDataList(ArrayList<Data> dataList) {
        emptyListTextView.setText("");
        adapter.setData(dataList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateList(int id) {
        adapter.removeReading(OfflineActivity.this, id);
    }

    @Override
    public void showMoreDetails(Data data) {
        fragmentContainer.setClickable(true);
        Fragment fragmentMail = MoreDetailsFragment.newInstance(data);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentMail, "MoreDetails");
        fragmentTransaction.commitAllowingStateLoss();
        getSupportActionBar().setTitle(getString(R.string.more_details_title));
    }

    private static final int EDIT_DATA_ITEM_REQUEST = 10;

    @Override
    public void editData(Data data) {
        Intent editIntent = new Intent(getApplicationContext(), DataCollectionActivity.class);
        editIntent.putExtra("Editing", data);
        startActivityForResult(editIntent, EDIT_DATA_ITEM_REQUEST);
    }

    @Override
    public void updateDataItemView(Data data) {
        adapter.updateReading(data);
    }

    @Override
    public void goLogin() {
        String username = MySingleton.getmInstance(this).getUserName();
        String ip = MySingleton.getmInstance(this).getUserIP();
        MySingleton.getmInstance(this).logout();
        MySingleton.getmInstance(this).saveUserIp(ip);
        MySingleton.getmInstance(this).saveUserName(username);
        DataDbHelper.getmInstance(this).deleteDistricts();
        Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent1);
    }

    @Override
    public void sendBroadCast(Intent intent) {
        sendBroadcast(intent);
    }

    @Override
    public void onListItemClick(Data data, View view) {
        presenter.onListItemClicked(getApplicationContext(), data, view);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, EDIT_DATA_ITEM_REQUEST, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Fragment child = (MoreDetailsFragment) getSupportFragmentManager().findFragmentByTag("MoreDetails");
        if (child != null) {
            getSupportFragmentManager().beginTransaction().
                    remove(getSupportFragmentManager().findFragmentByTag("MoreDetails")).commit();
            fragmentContainer.setClickable(false);
            getSupportActionBar().setTitle(getString(R.string.offline));
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Fragment child = (MoreDetailsFragment) getSupportFragmentManager().findFragmentByTag("MoreDetails");
            if (child != null) {
                getSupportFragmentManager().beginTransaction().
                        remove(getSupportFragmentManager().findFragmentByTag("MoreDetails")).commit();
                fragmentContainer.setClickable(false);
                getSupportActionBar().setTitle(getString(R.string.offline));
                return true;
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
