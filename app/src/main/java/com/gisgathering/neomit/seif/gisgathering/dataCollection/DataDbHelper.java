package com.gisgathering.neomit.seif.gisgathering.dataCollection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sief on 7/29/2018.
 */

public class DataDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "gis_gathering";
    private static final int DATABASE_VERSION = 10;
    private static DataDbHelper mInstance;

    private DataDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DataDbHelper getmInstance(Context context) {
        if (mInstance == null)
            mInstance = new DataDbHelper(context);

        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_DATA_TABLE = "CREATE TABLE " + DataContract.DataEntry.TABLE_NAME + " ( " +
                DataContract.DataEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DataContract.DataEntry.PLATE_NUMBER + " TEXT, " +
                DataContract.DataEntry.HOUSE_CONNECTION_NUMBER + " TEXT, " +
                DataContract.DataEntry.AMR_METER_BARCODE + " TEXT, " +
                DataContract.DataEntry.WATER_METER_NUMBER + " TEXT, " +
                DataContract.DataEntry.WATER_METER_READING + " TEXT, " +
                DataContract.DataEntry.REMARKS + " INTEGER, " +
                DataContract.DataEntry.WATER_METER_STATUS + " INTEGER, " +
                DataContract.DataEntry.METER_TYPE + " INTEGER, " +
                DataContract.DataEntry.METER_DIAMETER + " INTEGER, " +
                DataContract.DataEntry.REDUCER_DIAMETER + " INTEGER, " +
                DataContract.DataEntry.OPEN_CLOSE + " INTEGER," +
                DataContract.DataEntry.MECH_ELEC + " INTEGER," +
                DataContract.DataEntry.VALVE_TYPE + " INTEGER," +
                DataContract.DataEntry.COVER_EXISTS + " INTEGER," +
                DataContract.DataEntry.POSITION + " INTEGER," +
                DataContract.DataEntry.METER_TYPE_BOX + " INTEGER," +
                DataContract.DataEntry.PIPE_AFTER_METER + " INTEGER," +
                DataContract.DataEntry.INSIDE_OUTSIDE + " INTEGER," +
                DataContract.DataEntry.PIPE_MATERIAL + " INTEGER," +
                DataContract.DataEntry.PIPE_SIZE + " INTEGER," +
                DataContract.DataEntry.SEWER_CONNECTION + " INTEGER," +
                DataContract.DataEntry.NUMBER_OF_ELECTRIC_METERS + " TEXT," +
                DataContract.DataEntry.ELECTRIC_METER_NUMBER_LOWEST + " TEXT," +
                DataContract.DataEntry.POSTAL_CODE + " TEXT," +
                DataContract.DataEntry.NUMBER_OF_FLOORS + " TEXT," +
                DataContract.DataEntry.BUILDING_CATEGORY + " INTEGER," +
                DataContract.DataEntry.SUB_BUILDING_COMMERCIAL + " INTEGER," +
                DataContract.DataEntry.SUB_BUILDING_RESIDENTIAL + " INTEGER," +
                DataContract.DataEntry.SUB_BUILDING_GOVERNMENT + " INTEGER," +
                DataContract.DataEntry.DISTRICT + " INTEGER," +
                DataContract.DataEntry.DISTRICT_NAME + " TEXT," +
                DataContract.DataEntry.WASEL_NUMBER + " TEXT," +
                DataContract.DataEntry.AMANA_BARCODE + " TEXT," +
                DataContract.DataEntry.PARCEL_ID + " TEXT," +
                DataContract.DataEntry.OLD_ACCOUNT_NUMBER + " TEXT," +
                DataContract.DataEntry.PARTENER_NUMBER + " TEXT," +
                DataContract.DataEntry.NUMBER_OF_WATER_CONNECTIONS + " TEXT," +
                DataContract.DataEntry.NUMBER_OF_UNITS + " TEXT," +
                DataContract.DataEntry.ZONE_CODE + " TEXT," +
                DataContract.DataEntry.LOCATION + " TEXT," +
                DataContract.DataEntry.METER_CONNECTION_FULL_VIEW_IMG + " TEXT," +
                DataContract.DataEntry.METER_CLOSE_UP_VIEW_IMG + " TEXT," +
                DataContract.DataEntry.ELECTRIC_METER_IMG + " TEXT," +
                DataContract.DataEntry.PROPERTY_IMG + " TEXT," +
                DataContract.DataEntry.AMANA_PLATE_IMG + " TEXT," +
                DataContract.DataEntry.NWC_PLATE_IMG + " TEXT" +
                " );";

        String SQL_CREATE_DISTRICTS_TABLE = "CREATE TABLE " + DataContract.DataEntry.TABLE_NAME_1 + " ( " +
                DataContract.DataEntry.DISTRICT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DataContract.DataEntry.DISTRICT_ID_1 + " TEXT ," +
                DataContract.DataEntry.DISTRICT_NAME_1 + " TEXT " +
                " );";

        db.execSQL(SQL_CREATE_DATA_TABLE);
        db.execSQL(SQL_CREATE_DISTRICTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DataContract.DataEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + DataContract.DataEntry.TABLE_NAME_1);
        onCreate(db);
    }

    public boolean addNewData(Data data) {
        if (data == null) return false;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataContract.DataEntry.PLATE_NUMBER, data.getPlateNumber());
        values.put(DataContract.DataEntry.HOUSE_CONNECTION_NUMBER, data.getHouseConnNumber());
        values.put(DataContract.DataEntry.AMR_METER_BARCODE, data.getAmrMeterBarcode());
        values.put(DataContract.DataEntry.WATER_METER_NUMBER, data.getWaterMeterNumber());
        values.put(DataContract.DataEntry.WATER_METER_READING, data.getWaterMeterReading());
        values.put(DataContract.DataEntry.REMARKS, data.getRemarks());
        values.put(DataContract.DataEntry.WATER_METER_STATUS, data.getWaterMeterStatus());
        values.put(DataContract.DataEntry.METER_TYPE, data.getMeterType());
        values.put(DataContract.DataEntry.METER_DIAMETER, data.getMeterDiameter());
        values.put(DataContract.DataEntry.REDUCER_DIAMETER, data.getReducerDiameter());
        values.put(DataContract.DataEntry.OPEN_CLOSE, data.getOpenClose());
        values.put(DataContract.DataEntry.MECH_ELEC, data.getMechElec());
        values.put(DataContract.DataEntry.VALVE_TYPE, data.getValveType());
        values.put(DataContract.DataEntry.COVER_EXISTS, data.getCoverExists());
        values.put(DataContract.DataEntry.POSITION, data.getPosition());
        values.put(DataContract.DataEntry.METER_TYPE_BOX, data.getMeterTypeBox());
        values.put(DataContract.DataEntry.PIPE_AFTER_METER, data.getPipeAfterMeter());
        values.put(DataContract.DataEntry.INSIDE_OUTSIDE, data.getInsideOutside());
        values.put(DataContract.DataEntry.PIPE_MATERIAL, data.getPipeMaterial());
        values.put(DataContract.DataEntry.PIPE_SIZE, data.getPipeSize());
        values.put(DataContract.DataEntry.SEWER_CONNECTION, data.getSewerConnection());
        values.put(DataContract.DataEntry.NUMBER_OF_ELECTRIC_METERS, data.getNumberOfElectricMeters());
        values.put(DataContract.DataEntry.ELECTRIC_METER_NUMBER_LOWEST, data.getElectricMeterNumberLowest());
        values.put(DataContract.DataEntry.POSTAL_CODE, data.getPostalCode());
        values.put(DataContract.DataEntry.NUMBER_OF_FLOORS, data.getNumberOfFloors());
        values.put(DataContract.DataEntry.BUILDING_CATEGORY, data.getBuildingCategories());
        values.put(DataContract.DataEntry.SUB_BUILDING_COMMERCIAL, data.getCommercial());
        values.put(DataContract.DataEntry.SUB_BUILDING_RESIDENTIAL, data.getResidential());
        values.put(DataContract.DataEntry.SUB_BUILDING_GOVERNMENT, data.getGovernment());
        values.put(DataContract.DataEntry.DISTRICT, data.getDistrict());
        values.put(DataContract.DataEntry.DISTRICT_NAME, data.getDistrictName());
        values.put(DataContract.DataEntry.WASEL_NUMBER, data.getWaselNumber());
        values.put(DataContract.DataEntry.AMANA_BARCODE, data.getAmanaBarcode());
        values.put(DataContract.DataEntry.PARCEL_ID, data.getParcelID());
        values.put(DataContract.DataEntry.OLD_ACCOUNT_NUMBER, data.getOldAccountNumber());
        values.put(DataContract.DataEntry.PARTENER_NUMBER, data.getPartnerNumber());
        values.put(DataContract.DataEntry.NUMBER_OF_WATER_CONNECTIONS, data.getNumberOfWaterConnections());
        values.put(DataContract.DataEntry.NUMBER_OF_UNITS, data.getNumberOfUnits());
        values.put(DataContract.DataEntry.ZONE_CODE, data.getZoneCode());
        values.put(DataContract.DataEntry.LOCATION, data.getLocation());
        values.put(DataContract.DataEntry.METER_CONNECTION_FULL_VIEW_IMG, data.getPhotos().get("meterConnectionFullView"));
        values.put(DataContract.DataEntry.METER_CLOSE_UP_VIEW_IMG, data.getPhotos().get("meterCloseUpView"));
        values.put(DataContract.DataEntry.ELECTRIC_METER_IMG, data.getPhotos().get("electricMeterPhoto"));
        values.put(DataContract.DataEntry.PROPERTY_IMG, data.getPhotos().get("propertyPhoto"));
        values.put(DataContract.DataEntry.AMANA_PLATE_IMG, data.getPhotos().get("amanaPlatePhoto"));
        values.put(DataContract.DataEntry.NWC_PLATE_IMG, data.getPhotos().get("NWCPlatePhoto"));

        db.insert(DataContract.DataEntry.TABLE_NAME, null, values);
        db.close();
        return true;
    }

    public boolean updateData(Data data) {
        if (data == null) return false;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataContract.DataEntry.ID, data.getId());
        values.put(DataContract.DataEntry.PLATE_NUMBER, data.getPlateNumber());
        values.put(DataContract.DataEntry.HOUSE_CONNECTION_NUMBER, data.getHouseConnNumber());
        values.put(DataContract.DataEntry.AMR_METER_BARCODE, data.getAmrMeterBarcode());
        values.put(DataContract.DataEntry.WATER_METER_NUMBER, data.getWaterMeterNumber());
        values.put(DataContract.DataEntry.WATER_METER_READING, data.getWaterMeterReading());
        values.put(DataContract.DataEntry.REMARKS, data.getRemarks());
        values.put(DataContract.DataEntry.WATER_METER_STATUS, data.getWaterMeterStatus());
        values.put(DataContract.DataEntry.METER_TYPE, data.getMeterType());
        values.put(DataContract.DataEntry.METER_DIAMETER, data.getMeterDiameter());
        values.put(DataContract.DataEntry.REDUCER_DIAMETER, data.getReducerDiameter());
        values.put(DataContract.DataEntry.OPEN_CLOSE, data.getOpenClose());
        values.put(DataContract.DataEntry.MECH_ELEC, data.getMechElec());
        values.put(DataContract.DataEntry.VALVE_TYPE, data.getValveType());
        values.put(DataContract.DataEntry.COVER_EXISTS, data.getCoverExists());
        values.put(DataContract.DataEntry.POSITION, data.getPosition());
        values.put(DataContract.DataEntry.METER_TYPE_BOX, data.getMeterTypeBox());
        values.put(DataContract.DataEntry.PIPE_AFTER_METER, data.getPipeAfterMeter());
        values.put(DataContract.DataEntry.INSIDE_OUTSIDE, data.getInsideOutside());
        values.put(DataContract.DataEntry.PIPE_MATERIAL, data.getPipeMaterial());
        values.put(DataContract.DataEntry.PIPE_SIZE, data.getPipeSize());
        values.put(DataContract.DataEntry.SEWER_CONNECTION, data.getSewerConnection());
        values.put(DataContract.DataEntry.NUMBER_OF_ELECTRIC_METERS, data.getNumberOfElectricMeters());
        values.put(DataContract.DataEntry.ELECTRIC_METER_NUMBER_LOWEST, data.getElectricMeterNumberLowest());
        values.put(DataContract.DataEntry.POSTAL_CODE, data.getPostalCode());
        values.put(DataContract.DataEntry.NUMBER_OF_FLOORS, data.getNumberOfFloors());
        values.put(DataContract.DataEntry.BUILDING_CATEGORY, data.getBuildingCategories());
        values.put(DataContract.DataEntry.SUB_BUILDING_COMMERCIAL, data.getCommercial());
        values.put(DataContract.DataEntry.SUB_BUILDING_RESIDENTIAL, data.getResidential());
        values.put(DataContract.DataEntry.SUB_BUILDING_GOVERNMENT, data.getGovernment());
        values.put(DataContract.DataEntry.DISTRICT, data.getDistrict());
        values.put(DataContract.DataEntry.DISTRICT_NAME, data.getDistrictName());
        values.put(DataContract.DataEntry.WASEL_NUMBER, data.getWaselNumber());
        values.put(DataContract.DataEntry.AMANA_BARCODE, data.getAmanaBarcode());
        values.put(DataContract.DataEntry.PARCEL_ID, data.getParcelID());
        values.put(DataContract.DataEntry.OLD_ACCOUNT_NUMBER, data.getOldAccountNumber());
        values.put(DataContract.DataEntry.PARTENER_NUMBER, data.getPartnerNumber());
        values.put(DataContract.DataEntry.NUMBER_OF_WATER_CONNECTIONS, data.getNumberOfWaterConnections());
        values.put(DataContract.DataEntry.NUMBER_OF_UNITS, data.getNumberOfUnits());
        values.put(DataContract.DataEntry.ZONE_CODE, data.getZoneCode());
        values.put(DataContract.DataEntry.LOCATION, data.getLocation());
        values.put(DataContract.DataEntry.METER_CONNECTION_FULL_VIEW_IMG, data.getPhotos().get("meterConnectionFullView"));
        values.put(DataContract.DataEntry.METER_CLOSE_UP_VIEW_IMG, data.getPhotos().get("meterCloseUpView"));
        values.put(DataContract.DataEntry.ELECTRIC_METER_IMG, data.getPhotos().get("electricMeterPhoto"));
        values.put(DataContract.DataEntry.PROPERTY_IMG, data.getPhotos().get("propertyPhoto"));
        values.put(DataContract.DataEntry.AMANA_PLATE_IMG, data.getPhotos().get("amanaPlatePhoto"));
        values.put(DataContract.DataEntry.NWC_PLATE_IMG, data.getPhotos().get("NWCPlatePhoto"));
        db.update(DataContract.DataEntry.TABLE_NAME, values, DataContract.DataEntry.ID + " = ?",
                new String[]{String.valueOf(data.getId())});

        db.close();

        return true;
    }

    public ArrayList<Data> getAllData() {
        ArrayList<Data> dataList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DataContract.DataEntry.TABLE_NAME + " ORDER BY " +
                DataContract.DataEntry.ID + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Map<String, String> photos = new LinkedHashMap<>();
                photos.put("meterConnectionFullView", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.METER_CONNECTION_FULL_VIEW_IMG)));
                photos.put("meterCloseUpView", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.METER_CLOSE_UP_VIEW_IMG)));
                photos.put("electricMeterPhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ELECTRIC_METER_IMG)));
                photos.put("propertyPhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PROPERTY_IMG)));
                photos.put("amanaPlatePhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMANA_PLATE_IMG)));
                photos.put("NWCPlatePhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NWC_PLATE_IMG)));
                /*
                    int id, String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, Map<String, String> photos
                 */

                dataList.add(new Data(cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.ID)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PLATE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.HOUSE_CONNECTION_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMR_METER_BARCODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_READING)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.REMARKS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.REDUCER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.OPEN_CLOSE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.MECH_ELEC)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.VALVE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.COVER_EXISTS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.POSITION)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_TYPE_BOX)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_AFTER_METER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.INSIDE_OUTSIDE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_SIZE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SEWER_CONNECTION)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_ELECTRIC_METERS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ELECTRIC_METER_NUMBER_LOWEST)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.POSTAL_CODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_FLOORS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.BUILDING_CATEGORY)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_COMMERCIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_RESIDENTIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_GOVERNMENT)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WASEL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMANA_BARCODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PARCEL_ID)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.OLD_ACCOUNT_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PARTENER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_WATER_CONNECTIONS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_UNITS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ZONE_CODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.LOCATION)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT_NAME)),
                        photos));

            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return dataList;
    }

    public int dataCount() {
        ArrayList<Data> dataList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DataContract.DataEntry.TABLE_NAME + " ORDER BY " +
                DataContract.DataEntry.ID + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Map<String, String> photos = new LinkedHashMap<>();
                photos.put("meterConnectionFullView", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.METER_CONNECTION_FULL_VIEW_IMG)));
                photos.put("meterCloseUpView", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.METER_CLOSE_UP_VIEW_IMG)));
                photos.put("electricMeterPhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ELECTRIC_METER_IMG)));
                photos.put("propertyPhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PROPERTY_IMG)));
                photos.put("amanaPlatePhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMANA_PLATE_IMG)));
                photos.put("NWCPlatePhoto", cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NWC_PLATE_IMG)));
                /*
                    int id, String plateNumber, String houseConnNumber, String amrMeterBarcode, String waterMeterNumber,
                String waterMeterReading, int remarks, int waterMeterStatus, int meterType, int meterDiameter, int reducerDiameter,
                int openClose, int mechElec, int valveType, int coverExists, int position, int meterTypeBox, int pipeAfterMeter,
                int insideOutside, int pipeMaterial, int pipeSize, int sewerConnection, String numberOfElectricMeters,
                String electricMeterNumberLowest, String postalCode, String numberOfFloors, int buildingCategories,
                int commercial, int residential, int government, int district, String waselNumber, String amanaBarcode,
                String parcelID, String oldAccountNumber, String partnerNumber, String numberOfWaterConnections,
                String numberOfUnits, Map<String, String> photos
                 */
                dataList.add(new Data(cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.ID)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PLATE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.HOUSE_CONNECTION_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMR_METER_BARCODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_READING)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.REMARKS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.WATER_METER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.REDUCER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.OPEN_CLOSE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.MECH_ELEC)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.VALVE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.COVER_EXISTS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.POSITION)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.METER_TYPE_BOX)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_AFTER_METER)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.INSIDE_OUTSIDE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.PIPE_SIZE)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SEWER_CONNECTION)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_ELECTRIC_METERS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ELECTRIC_METER_NUMBER_LOWEST)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.POSTAL_CODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_FLOORS)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.BUILDING_CATEGORY)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_COMMERCIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_RESIDENTIAL)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.SUB_BUILDING_GOVERNMENT)),
                        cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.WASEL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.AMANA_BARCODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PARCEL_ID)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.OLD_ACCOUNT_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.PARTENER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_WATER_CONNECTIONS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.NUMBER_OF_UNITS)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.ZONE_CODE)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.LOCATION)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT_NAME)),
                        photos));

            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return dataList.size();
    }

    public void removeById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DataContract.DataEntry.TABLE_NAME, DataContract.DataEntry.ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public boolean addDistricts(ArrayList<District> districts) {
        if (districts == null) return false;
        if (districts.isEmpty()) return false;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values;
        long id;
        int counter = 0;
        for (int i = 0; i < districts.size(); i++) {
            values = new ContentValues();
            values.put(DataContract.DataEntry.DISTRICT_ID_1, districts.get(i).getId() + "");
            values.put(DataContract.DataEntry.DISTRICT_NAME_1, districts.get(i).getName());
            id = db.insert(DataContract.DataEntry.TABLE_NAME_1, null, values);
            if (id == -1)
                counter++;
        }
        db.close();
        if (counter == districts.size()) return false;

        return true;
    }

    public ArrayList<District> getDistricts() {
        ArrayList<District> districts = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DataContract.DataEntry.TABLE_NAME_1 + " ORDER BY " +
                DataContract.DataEntry.DISTRICT_ID + " ASC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                districts.add(new District(cursor.getInt(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT_ID_1)),
                        cursor.getString(cursor.getColumnIndex(DataContract.DataEntry.DISTRICT_NAME_1))));

            } while (cursor.moveToNext());
        }

        return districts;
    }

    public void deleteDistricts() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + DataContract.DataEntry.TABLE_NAME_1);
        db.close();
    }

}
