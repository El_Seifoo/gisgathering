package com.gisgathering.neomit.seif.gisgathering.offline.details;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gisgathering.neomit.seif.gisgathering.R;

import java.util.ArrayList;

/**
 * Created by sief on 7/30/2018.
 */

public class MoreDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_EDIT_TEXT = 0;
    private static final int VIEW_TYPE_IMAGE = 1;

    private ArrayList<MoreDetails> moreDetailsList;
    private boolean hasImages;
    private int imgsCount;


    public void setMoreDetailsList(ArrayList<MoreDetails> moreDetailsList, boolean hasImages, int imgsCount) {
        this.moreDetailsList = moreDetailsList;
        this.hasImages = hasImages;
        this.imgsCount = imgsCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (hasImages)
            if (position <= (moreDetailsList.size() - 1) && position >= (moreDetailsList.size() - imgsCount))
                return VIEW_TYPE_IMAGE;
        return VIEW_TYPE_EDIT_TEXT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_EDIT_TEXT:
                return new EditTexts(LayoutInflater.from(parent.getContext()).inflate(R.layout.more_details_edit_texts_list_item, parent, false));
            case VIEW_TYPE_IMAGE:
            default:
                return new Images(LayoutInflater.from(parent.getContext()).inflate(R.layout.more_details_images_list_item, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_EDIT_TEXT:
                ((EditTexts) holder).textView.setText(moreDetailsList.get(position).getLabel());
                ((EditTexts) holder).editText.setText(moreDetailsList.get(position).getValue());
                break;
            case VIEW_TYPE_IMAGE:
                ((Images) holder).label.setText(moreDetailsList.get(position).getLabel());
                Glide.with(((Images) holder).itemView.getContext())
                        .asBitmap()
                        .load(stringToBitmap(moreDetailsList.get(position).getValue().split("//////")[0]))
                        .apply(new RequestOptions().error(R.mipmap.logo))
                        .into(((Images) holder).imageView);

        }
    }

    public Bitmap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return moreDetailsList != null ? moreDetailsList.size() : 0;
    }

    public class EditTexts extends RecyclerView.ViewHolder {
        TextView textView;
        EditText editText;

        public EditTexts(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.label);
            editText = (EditText) itemView.findViewById(R.id.value);
        }
    }

    public class Images extends RecyclerView.ViewHolder {
        TextView label;
        ImageView imageView;

        public Images(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.label);
            imageView = (ImageView) itemView.findViewById(R.id.value);
        }
    }
}
