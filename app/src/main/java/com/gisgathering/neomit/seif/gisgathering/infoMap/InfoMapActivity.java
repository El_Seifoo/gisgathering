package com.gisgathering.neomit.seif.gisgathering.infoMap;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gisgathering.neomit.seif.gisgathering.R;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.Data;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataCollectionActivity;
import com.gisgathering.neomit.seif.gisgathering.dataCollection.DataDbHelper;
import com.gisgathering.neomit.seif.gisgathering.login.LoginActivity;
import com.gisgathering.neomit.seif.gisgathering.utils.MySingleton;
import com.google.zxing.integration.android.IntentIntegrator;

public class InfoMapActivity extends AppCompatActivity implements InfoMapMVP.View {
    private EditText plateNumber;
    private Button infoMapButton;
    private ProgressBar progressBar;
    private InfoMapMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_map);
        getSupportActionBar().setTitle(getIntent().hasExtra("GetInfo") ? getString(R.string.get_info) : getString(R.string.map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new InfoMapPresenter(this, new InfoMapModel());

        plateNumber = (EditText) findViewById(R.id.plate_number);
        plateNumber.setText(getIntent().hasExtra("plateNumber") ? getIntent().getStringExtra("plateNumber") : "");

        infoMapButton = (Button) findViewById(R.id.info_map_button);

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        infoMapButton.setText(getIntent().hasExtra("GetInfo") ? getString(R.string.get_info) : getString(R.string.map));

        infoMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestInfoMap(getApplicationContext(), plateNumber.getText().toString().trim(), getIntent().hasExtra("GetInfo"));
            }
        });
    }

    public void scanQR(View view) {
        presenter.requestQRScanner(this);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLocation(double longitude, double latitude) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + longitude + "," + latitude));
        startActivity(intent);
    }

    @Override
    public void initializeScanner(IntentIntegrator intentIntegrator) {
        intentIntegrator.initiateScan();
    }

    @Override
    public void handleQRScannerResult(String result) {
        plateNumber.setText(result.replaceAll("[^0-9]", ""));
    }

    @Override
    public void showInfo(Data data) {
        Intent intent = new Intent(getApplicationContext(), DataCollectionActivity.class);
        intent.putExtra("Editing", data);
        intent.putExtra("NotOffline", "NotOffline");
        startActivity(intent);
    }

    @Override
    public void goLogin() {
        String username = MySingleton.getmInstance(this).getUserName();
        String ip = MySingleton.getmInstance(this).getUserIP();
        MySingleton.getmInstance(this).logout();
        MySingleton.getmInstance(this).saveUserIp(ip);
        MySingleton.getmInstance(this).saveUserName(username);
        DataDbHelper.getmInstance(this).deleteDistricts();
        Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(getApplicationContext(), requestCode, resultCode, data);
    }


}
